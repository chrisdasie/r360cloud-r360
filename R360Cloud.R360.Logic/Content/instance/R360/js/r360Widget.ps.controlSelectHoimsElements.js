/**
Provides a selectedText function for HOIMS elements that concantenates element numbers.

@class r360Widget.controlSelectHoimsElements
@extends radient360Widget.controlSelect
*/
$.widget("r360Widget.controlSelectHoimsElements", $.radient360Widget.controlSelect, /** @lends r360Widget.controlSelectHoimsElements */ {
    options: {
    },

    _selectedTextFunc: function () {
        var that = this;
        return function (options, select) {
            if (options.length > 0) {
                if (options.length > 1) {
                    return that.__selectedText(options);
                } else {
                    return options[0].value;
                }
            }
            return that._nonSelectedText();
        }
    },

    _nonSelectedText: function () {
        return this.__selectedText(this.element.find('option'));
    },

    __selectedText: function (items) {
        var text = "Elements ";
        $.each(items, function (i, item) {
            text += items[i].value.split('-')[0].split('Element ')[1] + ' ';
        });
        return text;
    }
});