/** 
This panel is very basic in that it just sets its contents to the set of widgets
provided in the options. Each widget is wrapped in a div with a class for styling.

@class r360Panel.editKnownIssueLinkPanel 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.editKnownIssueLinkPanel", $.radient360Panel.formPanel, /** @lends r360Panel.editKnownIssueLinkPanel */ {
    options: {
        addSave: true,
        contents: []
    },

    _create: function () {
        var that = this;
        this._super();
        this.element.find(".cancel").text('Back');
        this.__createContents();
    },

    _setOption: function (key, value) {
        this._super(key, value);
        if (key == 'contents') {
            this.element.find('.panel-body').empty();
            this.element.find('.panel-body').append(this.getPanelBodyContentString());
            this.__createContents();
        }
    },

    getPanelBodyContentString: function () {
        var that = this;
        var body = this._super();
        body += "<form>";
        $.each(this.options.contents, function (i) {
            body += that.__contentElement(i);
        });
        body += "</form>";
        return body;
    },

    _resize: function () {
        this._super();
        var that = this;
        this.element.find("." + this.contextClass + '-widget').each(function (i, widget) {
            $(widget)[that.options.contents[i].type]("refresh");
        });
    },

    _destroy: function () {
        this._super();
    },
    
    _save: function () {
        var $modal = this.element.closest(".radient360Widget-modal");
        if ($modal.length > 0) {
            $modal.modal("close");
        }
        else {
            this._super();
        }
    },

    __contentElement: function (index) {
        var wrapperClass = this.contextClass + "-widget-wrapper";
        return '<div class="' + wrapperClass + ' ' + wrapperClass + '-' + index + ((index == this.options.contents.length - 1) ? ' last-wrapper' : "") +
            '"><div class="' + this.contextClass + '-widget"></div></div>';
    },

    __createContents: function () {
        var that = this;
        this.element.find("." + this.contextClass + '-widget').each(function (i, widget) {
            $(widget)[that.options.contents[i].type](that.options.contents[i].options);
        });
        this.element.resize();
    }
});