/** 
This class has the logic required to implement the PS add known issue workflow formset.

@class r360Panel.editKnownIssueFormPanel 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.editKnownIssueFormPanel", $.radient360Panel.formPanel, /** @lends r360Panel.editKnownIssueFormPanel */ {
    options: {
        formset: false,
        addNext: true,
        data: { text: null },
        nextPanel: null
    },
    
    _create: function () {
        this._super();
        this.element.closest(".radient360Widget-modal").addClass('col-sm-8');
    },

    getPanelBodyContentString: function () {
        var that = this;
        var body = this._super();
        body += "<form><table class='" + this.contextClass + "-table'><tbody>";
        body += "<tr><td><label>Note for Report:</label></td><td><textarea rows=8>" + (this.options.data.text ? this.options.data.text : "") + "</textarea></td></tr>";
        body += "</tbody></table></form>";
        return body;
    },

    _destroy: function () {
        this.element.empty();
        this._super();
    },

    _cancel: function () {
        // TODO handle modal better
        var $modal = this.element.closest(".radient360Widget-modal");
        if ($modal.length > 0) {
            $modal.modal("close");
        }
        else {
            this._super();
        }
    },
    
    _next: function (callback) {
        if (this.options.nextPanel) {
            this.element.closest(".radient360Widget-modal").removeClass('col-sm-8');
            this.element.closest(".radient360Widget-modal").addClass('col-sm-12');
            this._navigate(this.options.nextPanel.type, this.options.nextPanel.options);
        }
        callback();
    },

    show: function () {
        this.element.find(".cancel").text('Cancel');
        this.element.closest(".radient360Widget-modal").removeClass('col-sm-12');
        this.element.closest(".radient360Widget-modal").addClass('col-sm-8');
        this._super();
    }
});