/** 
This class has the logic required to implement the PV risk assessment workflow.
This is form 1 of 3 in the workflow. The navigation links between the forms are
configurable in the options to allow for maximum flexability but there may
still be dependencies between the panels that will break if they are not used
as a workflow unit.

@class r360Panel.pvRiskAssessmentFormPanel1 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.pvRiskAssessmentFormPanel1", $.radient360Panel.autoSaveFormPanel, /** @lends r360Panel.pvRiskAssessmentFormPanel1 */ {
    options: {
        widgetType: "pvRiskAssessmentFormPanel1",
        pvRiskAssessmentFormPanel1Class: "r360Panel-pvRiskAssessmentFormPanel1",
        addNext: true,
        modelUrl: "api/data/s/r360/pv/ra/continue/1",
        model: {
            // TODO: Remove this sample (draft) model of a Risk Assessment
            assets: ["assetId1", "assetId2"],
            notes: "Some notes about this risk assessment.",
            interval: { internal: {set: 24, recommended: 24}, external: {set:12,recommended:12} },
            degredationMechanisms: [{
                name: "Corrosion",
                justification: "Corrosion is insignificant at this time.",
                lof: { value: 3, justification: "This isn't very likely to worsen." },
                cof: { value: 3, justification: "No immediate effect." },
                rollUpToCircuit: false
            },
            {
                name: "Weld Attack",
                justification: "Welds performing well. All inspections complete.",
                lof: { value: 5, justification: "This is very unlikely." },
                cof: { value: 5, justification: "High Consequences should corrosion occur." },
                riskDriver: true,
                rollUpToCircuit: false
            },
            {
                name: "Bacteria",
                justification: "Bacteria tests within acceptable range.",
                lof: { value: 5, justification: "This is very unlikely." },
                cof: { value: 5, justification: "Severe consequences should bacteria occur in supply." },
                rollUpToCircuit: false
            }]
        },
        // TODO: Fetch these from the server.
        possibleDegredationMechanisms: ["Corrosion", "Bacteria", "Weld Attack"]
    },

    _create: function () {
        var that = this;
        this._super();
        this.element.addClass(this.options.pvRiskAssessmentFormPanel1Class);

        this.$degredationMechanismList = this.element.find('.degredationMechanismList').pvRiskAssessmentDegredationMechanisms({
            change : function() {
                // TODO: Add indication that something isn't saved.
                that._saveModel(function() {
                    // TODO: Remove indication that something isn't saved.
                });
            },
            possibleDegredationMechanisms: this.options.possibleDegredationMechanisms
        });
        this.$notes = this.element.find("textarea[name=notes]").on("change", function() {
            // TODO: Add indication that something isn't saved.
            that._saveModel(function() {
                // TODO: Remove indication that something isn't saved.
            });
        });
        this._writeFormData(this.options.model);
    },

    getPanelBodyContentString: function () {
        var that = this;
        var body = this._super();
        body += "<form>";
        body += "<fieldset><legend>Degredation Mechanisms</legend><div class='degredationMechanismList'></div></fieldset>";
        body += "<fieldset><legend>Notes</legend><textarea rows=5 name='notes'></textarea></fieldset>";
        body += "</form>";
        return body;
    },

    _readFormData: function() {
        var model = {}
        model.degredationMechanisms = radient360.page.callJQueryUiMethod(this.$degredationMechanismList, "values");
        model.notes = this.$notes.val();
        return model;
    },

    _writeFormData: function(model) {
        radient360.page.callJQueryUiMethod(this.$degredationMechanismList, "values", [model.degredationMechanisms]);
        this.$notes.val(model.notes);
    },

    _destroy: function () {
        this.element.removeClass(this.options.pvRiskAssessmentFormPanel1Class);
        this._super();
    }
});

/** 
This class has the logic required to implement the PV risk assessment workflow.
This is form 2 of 3 in the workflow. The navigation links between the forms are
configurable in the options to allow for maximum flexability but there may
still be dependencies between the panels that will break if they are not used
as a workflow unit.

@class r360Panel.pvRiskAssessmentFormPanel2 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.pvRiskAssessmentFormPanel2", $.radient360Panel.autoSaveFormPanel, /** @lends r360Panel.pvRiskAssessmentFormPanel2 */ {
    options: {
        widgetType: "pvRiskAssessmentFormPanel2",
        pvRiskAssessmentFormPanel2Class: "r360Panel-pvRiskAssessmentFormPanel2",
        addNext: true,
    },

    _create: function () {
        var that = this;
        this._super();
        this.element.addClass(this.options.pvRiskAssessmentFormPanel2Class);
        

        this.$riskDriverSelection = this.element.find(".riskDriverSelection").pvRiskAssessmentRiskDriver({
            change: function(e, data) {
                data = data.data;
                var riskDriver;
                for (var i = 0; data && !riskDriver && i < data.length; i++) {
                    if (data[i].riskDriver) {
                        riskDriver = data[i];
                        that.$intervalSelection.pvRiskAssessmentInterval("option", "riskDriver", riskDriver);
                    }
                }
               // TODO: Add indication that something isn't saved.
                that._saveModel(function() {
                    // TODO: Remove indication that something isn't saved.
                }); 
            },
            values: this.options.model.degredationMechanisms
        });

        var riskDriver;
        for (var i = 0; !riskDriver && i < this.options.model.degredationMechanisms.length; i++) {
            if (this.options.model.degredationMechanisms[i].riskDriver) {
                riskDriver = this.options.model.degredationMechanisms[i];
            }
        }
        this.$intervalSelection = this.element.find(".intervalSelection").pvRiskAssessmentInterval({
            riskDriver: riskDriver,
            change: function(e, data) {
               // TODO: Add indication that something isn't saved.
                that._saveModel(function() {
                    // TODO: Remove indication that something isn't saved.
                }); 
            }
        });

        this.$notes = this.element.find("textarea[name=notes]").on("change", function() {
           // TODO: Add indication that something isn't saved.
            that._saveModel(function() {
                // TODO: Remove indication that something isn't saved.
            }); 
        });
        this._writeFormData(this.options.model);
    },

    getPanelBodyContentString: function () {
        var that = this;
        var body = this._super();
        body += "<form>";
        body += "<fieldset><legend>Risk Driver Selection</legend>";
        body += "<div class='riskDriverSelection'></div>";
        body += "<div class='intervalSelection'></div>";
        body += "</fieldset>";
        body += "<fieldset><legend>Notes</legend><textarea rows=5 name='notes'></textarea></fieldset>";
        body += "</form>";
        return body;
    },

    _readFormData: function() {
        var model = {}
        model.degredationMechanisms = this.$riskDriverSelection.pvRiskAssessmentRiskDriver("values");
        model.interval = this.$intervalSelection.pvRiskAssessmentInterval("value");
        model.notes = this.$notes.val();
        return model;
    },

    _writeFormData: function(model) {
        var that = this;
        this.$riskDriverSelection.pvRiskAssessmentRiskDriver("values", model.degredationMechanisms);
        this.$intervalSelection.pvRiskAssessmentInterval("value", model.interval);
        this.$notes.val(model.notes);
    },

    refresh: function () {
        this._super();
        this.$intervalSelection.pvRiskAssessmentInterval("refresh");
    },

    _destroy: function () {
        this.element.removeClass(this.options.pvRiskAssessmentFormPanel2Class);
        this._super();
    }
});

/** 
This class has the logic required to implement the PV risk assessment workflow.
This is form 3 of 3 in the workflow. The navigation links between the forms are
configurable in the options to allow for maximum flexability but there may
still be dependencies between the panels that will break if they are not used
as a workflow unit.

@class r360Panel.pvRiskAssessmentFormPanel3 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.pvRiskAssessmentFormPanel3", $.radient360Panel.autoSaveFormPanel, /** @lends r360Panel.pvRiskAssessmentFormPanel3 */ {
    options: {
        widgetType: "pvRiskAssessmentFormPanel3",
        pvRiskAssessmentFormPanel3Class: "r360Panel-pvRiskAssessmentFormPanel3",
        addSave: true
    },

    _create: function () {
        this._super();
        this.element.addClass(this.options.pvRiskAssessmentFormPanel3Class);

        this.element.find(".equipment").tableDataView ({
            retrieveDataUrl: "api/data/s/pv/component/component_data",
            columns: [ { data : "tagId", title : "Tag ID", sortable : true, visible : true, type : "text" },
                       { data : "type", title : "Type", sortable : true, visible : true, type : "multiselect", values : [ "Circuit Piping", "Exchanger", "CO Pump Air Separator Tank", "COW Pump Air Separator Tank", "Cargo Oil Pump", "COW & Stripping Pump" ]},
                       { data : "description", title : "Description", sortable : true, visible : true, type : "text" }],
            actions: null,
            selection: 'multi'
        });

        this._writeFormData(this.options.model);
    },

    getPanelBodyContentString: function () {
        var that = this;
        var body = this._super();
        body += "<form>";
        body += "<fieldset><legend>Summary</legend><div class='summary'></div></fieldset>";
        body += "<fieldset><legend>Equipment</legend><div class='list-group'>Please select other Equipment to apply this Risk Assessment</div><div class='equipment'></div></fieldset>"
        body += "</form>";
        return body;
    },

    _readFormData: function() {
        var model = {}
        //        model.notes = this.element.find("textarea[name=notes]").val();
        return model;
    },

    _writeFormData: function(model) {
        var summaryString = "";
        summaryString += "<div>" + this.options.model.notes + "</div>";
        this.element.find(".summary").empty().append(summaryString);

        //        this.element.find("textarea[name=notes]").val(model.notes);
    },

    _save: function (callback) {
        var that = this;
        this._saveModel(function () {        
            that._cancel(callback);
        });
    },

    _destroy: function () {
        this.element.removeClass(this.options.pvRiskAssessmentFormPanel3Class);
        this._super();
    }
});


/** 
@class r360Widget.pvRiskAssessmentDegredationMechanisms 
*/
$.widget("r360Widget.pvRiskAssessmentDegredationMechanisms", /** @lends r360Widget.pvRiskAssessmentDegredationMechanisms */ {
    options: {
        widgetType: "pvRiskAssessmentDegredationMechanisms",
        pvRiskAssessmentDegredationMechanismsClass: "r360Widget-pvRiskAssessmentDegredationMechanisms",
        values: null,
        possibleDegredationMechanisms: null,
        riskLevelMap: [[0,0,0,0,1],
                       [0,0,0,1,2],
                       [0,1,1,2,2],
                       [1,1,2,2,3],
                       [1,2,3,3,3]]
    },

    _create: function () {
        var that = this;
        this._super();
        this.element.addClass(this.options.pvRiskAssessmentDegredationMechanismsClass);

        this.element.append(this._getContentString());

        this.$degredationMechanismList = this.element.find(".degredationMechanismList");
        this.$degredationMechanismEdit = this.element.find(".degredationMechanismEdit").hide();
        this.$degredationMechanismCofAssign = this.element.find(".degredationMechanismCofAssign").hide();

        // Start Edit
        this.$degredationMechanismList.on("click", ".glyphicon-pencil", function (e) {
            that._renderValue($(e.target).data('valueIndex'));
            that.$degredationMechanismEdit.show();
            that.$degredationMechanismEdit.find(".btn.delete").show();
            that.$degredationMechanismList.hide();
            radient360.page.callJQueryUiMethod(that.element.closest(".radient360Panel-formPanel"), "_disableButtons");
            that.element.resize();
        });

        // Start Assign COF
        this.$degredationMechanismList.on("click", ".glyphicon-link", function (e) {
            that._renderValue($(e.target).data('valueIndex'));
            that.$degredationMechanismCofAssign.show();
            that.$degredationMechanismList.hide();
            radient360.page.callJQueryUiMethod(that.element.closest(".radient360Widget-formPanel"), "_disableButtons");
            that.element.resize();
        });

        // Start Add
        this.$degredationMechanismList.find(".btn.add").on("click", function (e) {
            that._renderValue();
            that.$degredationMechanismEdit.show();
            that.$degredationMechanismEdit.find(".btn.delete").hide();
            that.$degredationMechanismList.hide();
            radient360.page.callJQueryUiMethod(that.element.closest(".radient360Panel-formPanel"), "_disableButtons");
            that.element.resize();
        });

        // Finish COF Assign
        this.$degredationMechanismCofAssign.find(".btn.ok,.btn.discard").on("click", function (e) {
            var changed = false;
            var closing = true;
            var $target = $(e.target);
            if ($target.hasClass("ok")) {
                var value = {};

                // Read COF from form.
                if (!value.cof) {
                    value.cof = {};
                }
                value.cof.value = that.$degredationMechanismCofAssign.find(".cofSlider").slider("value");
                value.cof.justification = that.$degredationMechanismCofAssign.find("textarea[name=cofJustification]").val();
                if (value.cof.value == 0) {
                    delete value.cof;
                }

                for (var i = 0; i < that.options.values.length; i++) {
                    that.options.values[i].cof = { value: value.cof.value, justification: value.cof.justification };
                }

                changed = true;
            }

            that.$degredationMechanismCofAssign.hide();
            that.$degredationMechanismList.show();
            radient360.page.callJQueryUiMethod(that.element.closest(".radient360Widget-formPanel"), "_enableButtons");
            if (changed) {
                for (var i = 0; i < that.options.values.length; i++) {
                    that.options.values[i].riskDriver = false;
                }
                that._renderValues();
                that._trigger('change', e, that.options.values);
            }
            that.element.resize();
        });

        // Finish Add/Edit
        this.$degredationMechanismEdit.find(".btn.delete").button({ text: "Delete", confirmRequired: true });
        this.$degredationMechanismEdit.find(".btn.ok,.btn.delete,.btn.discard").on("click", function (e) {
            var changed = false;
            var closing = true;
            var $target = $(e.target);
            if ($target.hasClass("ok")) {
                var value = {};
                if (that.currentValueIndex != undefined) {
                    value = that.options.values[that.currentValueIndex];
                } else {
                    that.options.values.push(value);
                }

                // Read Degredation Mechanism from form.
                value.name = that.$degredationMechanismEdit.find("select[name=degredationMechanism] option:selected").text();
                value.justification = that.$degredationMechanismEdit.find("textarea[name=degredationMechanismJustification]").val();


                // Read LOF from form.
                if (!value.lof) {
                    value.lof = {};
                }
                value.lof.value = that.$degredationMechanismEdit.find(".lofSlider").slider("value");
                value.lof.justification = that.$degredationMechanismEdit.find("textarea[name=lofJustification]").val();
                if (value.lof.value == 0) {
                    delete value.lof;
                }                    
                
                // Read COF from form.
                if (!value.cof) {
                    value.cof = {};
                }
                value.cof.value = that.$degredationMechanismEdit.find(".cofSlider").slider("value");
                value.cof.justification = that.$degredationMechanismEdit.find("textarea[name=cofJustification]").val();
                if (value.cof.value == 0) {
                    delete value.cof;   
                }

                // Read Roll Up to Corrosion Circuit Flag from form
                value.rollUpToCircuit = that.$degredationMechanismEdit.find(".rollUpToCircuit").is(':checked');

                changed = true;
            }
            if ($target.hasClass("delete")) {
                    if (that.currentValueIndex != undefined) {
                        that.options.values.splice(that.currentValueIndex, 1);
                        changed = true;
                    }
            }

            that.$degredationMechanismEdit.hide();
            that.$degredationMechanismList.show();
            radient360.page.callJQueryUiMethod(that.element.closest(".radient360Panel-formPanel"), "_enableButtons");
            if (changed) {
                for (var i = 0; i < that.options.values.length; i++) {
                    that.options.values[i].riskDriver = false;
                }
                that._renderValues();
                that._trigger('change', e, that.options.values);
            }
            that.element.resize();
        });

        var onSliderSlide = function (event, ui) {
            if (ui.value <= 0) {
                $(ui.handle).text("");
            } else {
                $(ui.handle).text(ui.value);
            }
            var lof = that.$degredationMechanismEdit.find(".lofSlider").slider("value")
            var cof = that.$degredationMechanismEdit.find(".cofSlider").slider("value")
            if ($(event.target).hasClass("lofSlider")) {
                lof = ui.value;
            }
            if ($(event.target).hasClass("cofSlider")) {
                cof = ui.value;
            }
            var riskLevel = "";
            if (lof && cof) {
                riskLevel += that.options.riskLevelMap[cof-1][lof-1];
            }
            that.$degredationMechanismEdit.find("select[name=degredationMechanism]").attr('class', 'riskLevel-' + riskLevel);
        };

        var onCofAssignSliderSlide = function (event, ui) {
            if (ui.value <= 0) {
                $(ui.handle).text("");
            } else {
                $(ui.handle).text(ui.value);
            }
            
            var cof = that.$degredationMechanismCofAssign.find(".cofSlider").slider("value")
            
            if ($(event.target).hasClass("cofSlider")) {
                cof = ui.value;
            }
        };

        var onSliderChange = function (event, ui) {
            if (ui.value <= 0) {
                $(ui.handle).text("");
            } else {
                $(ui.handle).text(ui.value);
            }  
        };

        this.$degredationMechanismEdit.find(".lofSlider,.cofSlider").slider({
            min: 0,
            max: 5,
            step: 1,
            slide: onSliderSlide,
            change: onSliderChange
        });

        this.$degredationMechanismCofAssign.find(".lofSlider,.cofSlider").slider({
            min: 0,
            max: 5,
            step: 1,
            slide: onCofAssignSliderSlide,
            change: onSliderChange
        });

        this._renderValues();
    },

    _getContentString: function() {
        var contents = "";
        
        contents += "<div class='degredationMechanismList'>" 
        contents += "<table class='table'><col class='name'/><col class='lof'/><col class='cof'/><col class='rollUpToCorrosionCircuit'/><col class='action'/>";
        contents += "<thead><tr><th class='name'>Name</th><th class='lof'>LOF</th><th class='cof'>COF <span class='glyphicon glyphicon-link btn.assignCof'></span></th><th class='rollUpToCorrosionCircuit'>Roll Up</th><th class='action'></th></tr></thead>";
        contents += "<tbody></tbody>";
        contents += "</table>";
        contents += "<div class='btn-group btn-group-justified' role='group' aria-label='...'><div type='button' class='btn btn-default add'>Add</div></div>";
        contents += "</div>";

        contents += "<form class='degredationMechanismEdit'>";
        
        contents += "<div role='group' class='fieldWithJustification'>"
        contents += "<label>Degredation Mechanism</label>";
        contents += "<div class='field'><select name=degredationMechanism>";
        contents += "</select></div>";
        contents += "<input type='checkbox' name='degradationMechanismRollToCorrosionCircuit' value='Yes' class='rollUpToCircuit'>Roll up to Corrosion Circuit";
        contents += "<textarea name='degredationMechanismJustification' placeholder='Justification...' rows=2></textarea>";
        contents += "</div>";

        contents += "<div role='group' class='fieldWithJustification'>"
        contents += "<label>LOF</label>";
        contents += "<div class='field'><div class='lofSlider'></div></div>";
        contents += "<textarea name='lofJustification' placeholder='Justification...' rows=2></textarea>";
        contents += "</div>";

        contents += "<div role='group' class='fieldWithJustification'>"
        contents += "<label>COF</label>";
        contents += "<div class='field'><div class='cofSlider'></div></div>";
        contents += "<textarea name='cofJustification' placeholder='Justification...' rows=2></textarea>";
        contents += "</div>";
        
        contents += "<div class='btn-group btn-group-justified' role='group' aria-label='...'>";
        contents += "<div type='button' class='btn btn-default discard'>Discard</div>";
        contents += "<div type='button' class='btn btn-default delete' data-original-text='Delete'>Delete</div>";
        contents += "<div type='button' class='btn btn-primary ok'>OK</div>";
        contents += "</div>";
        
        contents += "</form>";

        contents += "<form class='degredationMechanismCofAssign'>";

        contents += "<div role='group' class='fieldWithJustification'>"
        contents += "<label>COF</label>";
        contents += "<div class='field'><div class='cofSlider'></div></div>";
        contents += "<textarea name='cofJustification' placeholder='Justification...' rows=2></textarea>";
        contents += "</div>";

        contents += "<div class='btn-group btn-group-justified' role='group' aria-label='...'>";
        contents += "<div type='button' class='btn btn-default discard'>Discard</div>";
        contents += "<div type='button' class='btn btn-primary ok'>OK</div>";
        contents += "</div>";

        contents += "</form>";

        return contents;
    },

    _renderValues: function () {
        var $tbody = this.$degredationMechanismList.find("tbody");
        $tbody.empty();
        var values = this.options.values;
        if (values && values.length > 0) {
            var rows = "";
            for (var i = 0; i < values.length; i++) {
                var riskLevel = "";
                if (values[i].lof && values[i].lof.value && values[i].cof && values[i].cof.value) {
                    riskLevel += this.options.riskLevelMap[values[i].cof.value-1][values[i].lof.value-1];
                }
                rows += "<tr class='riskLevel-" + riskLevel + "'>";
                rows += "<td class='name'>" + values[i].name + "</td>";
                rows += "<td class='lof'>" + (values[i].lof && values[i].lof.value ? values[i].lof.value : "") + "</td>";
                rows += "<td class='cof'>" + (values[i].cof && values[i].cof.value ? values[i].cof.value : "") + "</td>";
                rows += "<td class='rollUpToCorrosionCircuit'>" + (values[i].rollUpToCircuit && values[i].rollUpToCircuit == true ? "Yes" : "No") + "</td>";
                rows += "<td class='action'><span class='glyphicon glyphicon-pencil' data-value-index=" + i + "></span></td>";
                rows += "</tr>";
            }
        }
        $tbody.append(rows);
    },

    _renderValue: function (index) {
        if (index != undefined) {
            this.currentValueIndex = index;
        } else {
            delete this.currentValueIndex;
        }

        var $edit = this.$degredationMechanismEdit;
        $edit.find("textarea").val("");
        $edit.find(".lofSlider,.cofSlider").slider("value", 0);
        var $select = $edit.find("select[name=degredationMechanism]").empty();
        for (var i = 0; i < this.options.possibleDegredationMechanisms.length; i++) {
            var include = true;
            for (var j = 0; j < this.options.values.length; j++) {
                if (this.options.possibleDegredationMechanisms[i] == this.options.values[j].name) {
                    include = false;
                    break;
                }
            }
            if (include) {
                $select.append("<option>" + this.options.possibleDegredationMechanisms[i] + "</option>");
            }
        }

        if (index != undefined && this.options.values && this.options.values.length > index) {

            var value = this.options.values[index];
            var riskLevel = "";
            if (value.lof && value.lof.value && value.cof && value.cof.value) {
                riskLevel += this.options.riskLevelMap[value.cof.value-1][value.lof.value-1];
            }
            $select.attr('class', 'riskLevel-' + riskLevel);
            if (value.name) {
                $select.prepend("<option selected>" + value.name + "</option>");
            }
            if (value.justification) {
                $edit.find("textarea[name=degredationMechanismJustification]").val(value.justification);
            }
            if (value.lof) {
                if (value.lof.value) {
                    $edit.find(".lofSlider").slider("value", value.lof.value);			    
                }
                if (value.lof.justification) {
                    $edit.find("textarea[name=lofJustification]").val(value.lof.justification);
                }
            }
            if (value.cof) {
                if (value.cof.value) {
                    $edit.find(".cofSlider").slider("value", value.cof.value);			    
                }
                if (value.cof.justification) {
                    $edit.find("textarea[name=cofJustification]").val(value.cof.justification);
                }
            }
            if (value.rollUpToCircuit != null) {
                var x = $edit.find(".rollUpToCircuit");
                $edit.find(".rollUpToCircuit").prop('checked', value.rollUpToCircuit);
            }
            else {
                $edit.find(".rollUpToCircuit").prop('checked', false);
            }
        }
    },

    values: function(values) {
        if (!values) {
            return this.options.values;
        }
        this.options.values = values;
        this._renderValues();
    },

    _destroy: function () {
        this.element.removeClass(this.options.pvRiskAssessmentDegredationMechanismsClass);
        this._super();
    }    
});

/** 
@class r360Widget.pvRiskAssessmentRiskDriver 
*/
$.widget("r360Widget.pvRiskAssessmentRiskDriver", /** @lends r360Widget.pvRiskAssessmentRiskDriver */ {
    options: {
        chartOptions: {
            colors: ['gray','green','orange','red'],
            chart: {type: 'bubble'},
            title: {text: null},
            legend: {enabled: false},
            exporting: {enabled: false},
            credits: {enabled: false},
            tooltip: {
                shared: true,
                useHTML: true,
                headerFormat: '<table>',
                footerFormat: '</table>',
            },
            
            plotOptions: {
                bubble: {
                    cursor: 'pointer',
                    maxSize: '15%',
                    minSize: '15%',
                    dataLabels: {
                        enabled: true,
                        //useHTML: true,
                        formatter: function() {
                            var content = "<span style='text-align: center; background-color: blue;'>";
                            var values = this.series.options.data[this.point.index][3];
                            for (var i = 0; i < values.length; i++) {
                                content += (i > 0 ? "<br/>": "") + values[i].name;
                            }
                            content += "</span>";
                            return values.length == 1 ? content : values.length;
                        }
                    },
                    states: {hover: {enabled: false}}
                },
                series: {point: {events: {click: function (e) {e.point.series.chart.tooltip.refresh(e.point, e);}}}}
            },
            xAxis: [{lineWidth: 1, lineColor: '#d8d8d8', tickWidth: 0, gridLineWidth: 1, title: {text: 'LOF'}, min: 1, max: 5, categories: ['NA','1','2','3','4','5']},
                    {lineWidth: 0, tickWidth: 0, title: {text: 'LOF'}, min: 1, max: 5, categories: ['NA','1','2','3','4','5'], linkedTo: 0, opposite: true}],
            yAxis: [{lineWidth: 1, lineColor: '#d8d8d8', tickWidth: 0, title: {text: 'COF'}, min: 1, max: 5, categories: ['NA','1','2','3','4','5']},
                    {lineWidth: 0, tickWidth: 0, title: {text: 'COF'}, min: 1, max: 5, categories: ['NA','1','2','3','4','5'], linkedTo: 0, opposite: true}]
        },
        seriesMap: [[0,0,0,0,1],
                    [0,0,0,1,2],
                    [0,1,1,2,2],
                    [1,1,2,2,3],
                    [1,2,3,3,3]],
        values: null
    },

    _create: function() {
        var that = this;
        this._super();
        var contentString = "";
        contentString += "<select name='selectRiskDriverSelection'></select>";
        contentString += "<div class='graphRiskDriverSelection'></div>";
        this.element.append(contentString);

        this.$graph = this.element.find(".graphRiskDriverSelection");
        this.$select = this.element.find("select[name=selectRiskDriverSelection]");

        this.pointFormatter = function() {
            var values = this.series.options.data[this.index][3];
            var rows = "";
            for (var i = 0; i < values.length; i++) {
                var value = values[i];
                var index = 0;
                for (; index < that.options.values.length; index++) {
                    if (value.name == that.options.values[index].name) {
                        break;
                    }
                }
                value = that.options.values[index];
                rows += "<tr><td><label>";
                rows += "<input type='radio' name='riskDriver'" + (value.riskDriver ? " checked": "") + " value='" + index + "' />&nbsp;";
                rows += value.name;
                rows += "</label></td></tr>";
            }
            return rows;
        };
        this.element.on('change', 'input[name=riskDriver]', function(e) {
            for (var i = 0; i < that.options.values.length; i++) {
                that.options.values[i].riskDriver = i == this.value;
            }
            that._trigger('change', e, {data: that.options.values});
            that._renderSelect();
        });
        this.$select.on('change', function(e) {
            for (var i = 0; i < that.options.values.length; i++) {
                that.options.values[i].riskDriver = i == this.value;
            }
            that._trigger('change', e, {data: that.options.values});
            that._renderSelect();
        });

        this._renderValues();
    },

    _renderValues: function() {
        this.seriesData = [];
        for (var i = 0; i < this.options.chartOptions.colors.length; i++) {
            this.seriesData.push({data:[]});
        }
        for (var lof = 1; lof <= 5; lof++) {
            for (var cof = 1; cof <= 5; cof++) {
                var values = [];
                // TODO: What if there are no values?
                for (var i = 0; i < this.options.values.length; i++) {
                    var value = this.options.values[i];
                    if (value.lof && value.lof.value && value.lof.value == lof &&
                        value.cof && value.cof.value && value.cof.value == cof) {
                        values.push(value);        
                    }
                }
                if (values.length > 0) {
                    this.seriesData[this.options.seriesMap[cof-1][lof-1]].data.push([lof,cof,values.length,values]);
                }
            }
        }
        this.chartOptions = $.extend(true, {}, this.options.chartOptions, {tooltip: {pointFormatter : this.pointFormatter}, series: this.seriesData});
        this.$graph.highcharts(this.chartOptions);
        this._renderSelect();
    },

    _renderSelect: function() {
        var selectContentString = "";
        var riskDriverRiskLevel = "";
        for (var i = 0; i < this.options.values.length; i++) {
            var value = this.options.values[i];
            var riskLevel = "";
            if (value.lof && value.lof.value && 
                value.cof && value.cof.value) {
                riskLevel += this.options.seriesMap[value.cof.value-1][value.lof.value-1];
                selectContentString += "<option class='riskLevel-" + riskLevel + "'" + (value.riskDriver ? " selected" : "") + " value='" + i + "'>" + value.name + "</option>";
                if (value.riskDriver) {
                    riskDriverRiskLevel = riskLevel;
                }
            }
        }
        this.$select.empty();
        this.$select.append(selectContentString);
        this.$select.attr('class', 'riskLevel-' + riskDriverRiskLevel);
    },

    values: function(values) {
        if (!values) {
            return this.options.values;
        }
        this.options.values = values;
        this._renderValues();
    }
});

/** 
@class r360Widget.pvRiskAssessmentInterval
*/
$.widget("r360Widget.pvRiskAssessmentInterval", /** @lends r360Widget.pvRiskAssessmentInterval */ {
    options: {
        value: null,
        lastInspection: {internal: moment(), external: moment() },
        riskDriver: null,
        riskLevelMap: [[0,0,0,0,1],
                       [0,0,0,1,2],
                       [0,1,1,2,2],
                       [1,1,2,2,3],
                       [1,2,3,3,3]],
        riskLevelIntervals: [{internal: 96, external: 48},
                             {internal: 72, external: 36},
                             {internal: 48, external: 24},
                             {internal: 24, external: 12}]
    },

    _create: function() {
        var that = this;
        this._super();
        if (this.options.riskDriver) {
            this._setOption("riskDriver", this.options.riskDriver);
        }
        this._renderValue();
    },

    _setOption: function(key, value) {
        this._super(key, value);
        if (key == 'riskDriver' && value.lof && value.lof.value && value.cof && value.cof.value) {
            var lof = value.lof.value;
            var cof = value.cof.value;
            var intervals = this.options.riskLevelIntervals[this.options.riskLevelMap[cof-1][lof-1]];
            this.value({internal: {recommended: intervals.internal, set: intervals.internal}, 
                        external: {recommended: intervals.external, set: intervals.external}});
        }  
    },

    _renderValue: function() {
        var that = this;
        var removed = false;
        if (this.element.heroBoxGroup("instance")) {
            this.element.heroBoxGroup("destroy");
            removed = true;
        }
        this.element.empty();

        if (!this.options.value || !this.options.value.internal || !this.options.value.external || !this.options.riskDriver) {
            if (removed) {
                this.element.resize();
            }
            return;
        }
        
        var nextInspection = {
            internal: moment(this.options.lastInspection.internal).add(this.options.value.internal.set, "months"),
            external: moment(this.options.lastInspection.external).add(this.options.value.external.set, "months")
        }

        this.element.heroBoxGroup({
            activeEnabled: false,
            heroBoxes: [{type: "intervalOverride", options: {
                            value: this.options.value.internal.set,
                            label: "Internal Interval",
                            change: function(e, data) {
                                that.options.value.internal.set = data;
                                that._renderValue();
                                that._trigger("change", e, that.options.value);
                            }}},
                        {type: "intervalOverride", options: {
                            value: this.options.value.external.set,
                            label: "External Interval",
                            change: function(e, data) {
                                that.options.value.external.set = data;
                                that._renderValue();
                                that._trigger("change", e, that.options.value);
                            }}},
                        {primary: nextInspection.internal.format("MMM D"), secondary: nextInspection.internal.format("YYYY"), tertiary: "Internal Date"},
                        {primary: nextInspection.external.format("MMM D"), secondary: nextInspection.external.format("YYYY"), tertiary: "External Date"}]   
        });
        this.element.heroBoxGroup("refresh");
        if (!removed) {
            this.element.resize();
        }
    },

    value: function(value) {
        if (!value) {
            return this.options.value;
        }
        this.options.value = value;
        this._renderValue();
    },

    refresh: function() {
        if (this.element.heroBoxGroup("instance")) {
                this.element.heroBoxGroup("refresh");
        }
    }
});

/** 
@class r360Widget.intervalOverride
*/
$.widget("r360Widget.intervalOverride", /** @lends r360Widget.intervalOverride */ {
    options: {
        pvRiskAssessmentIntervalOverrideClass: "pvRiskAssessmentIntervalOverrideClass",
        hbgBaseClass: "radient360Widget-heroBoxGroup",
        label: 'External',
        value: null
    },

    _create: function() {
        var that = this;
        this._super();
        this.element.addClass(this.options.pvRiskAssessmentIntervalOverrideClass);
        var contentString = "";
        contentString += "<div class='" + this.options.hbgBaseClass + "-item-primary'>";
        contentString += "<div class='primary-label'>"; 
        contentString += this.options.value;
        contentString += "</div><!-- primary-label -->";
        contentString += "<input type='number' step=12 min=12 max=120 maxlength=3 value='" + this.options.value + "'/>";
        contentString += "<div class='glyphicon glyphicon-pencil'></div>";
        contentString += "</div><!-- " + this.options.hbgBaseClass + "-item-primary -->";
        contentString += "<div class='" + this.options.hbgBaseClass + "-item-secondary'>";
        contentString += "Months";
        contentString += "</div><!-- " + this.options.hbgBaseClass + "-item-secondary -->";
        contentString += "<div class='" + this.options.hbgBaseClass + "-item-tertiary'>";
        contentString += this.options.label;
        contentString += "</div><!-- " + this.options.hbgBaseClass + "-item-tertiary -->";
        this.element.append(contentString);
        
        this.$label = this.element.find(".primary-label");
        this.$input = this.element.find("input[type=number]").hide();
        
        this.element.find(".glyphicon").on("click", function(e) {
            that.$label.hide();
            that.$input.show().focus();
        })
        this.$input.on("blur", function(e) {
            that.$label.show();
            that.$input.hide();
            that.$label.text(that.$input.val());
            that._trigger("change", e, that.$input.val());
        });
        this.$input.on("change", function(e) {
            //that.$label.text(that.$input.val());
            //that._trigger("change", e, that.$input.val());
        })
    },

    _destroy: function() {
        this.removeClass(this.options.pvRiskAssessmentIntervalOverrideClass);  
        this._super();
    }

});
