// Initialize the radient360 namespace, if not already defined.
var radient360 = radient360 || {};
radient360.module = radient360.modules || {};

radient360.module.r360 = (function ($) {
	'use strict';

	var exports = {

		preInit: function () {
			if (this.preInitialized == true) {
				return;
			}
			this.preInitialized = true;
			var defaultGetPanels = radient360.page.getPanels;
			radient360.page.getPanels = function (hash, callback) {
			    radient360.module.r360.getPanels(hash, function (rootPanelDef) {
			        if (rootPanelDef) {
			            callback(rootPanelDef);
			        } else {
			            defaultGetPanels(hash, callback);
			        }
			    });
			};
		},

		postInit: function () {
	
			if (this.postInitialized == true) {
				return;
			}
			this.postInitialized = true;

			//$("body").on("click", ".prototype-draftButton", function () {
			//    window.location.hash = window.location.hash.substr(0, 9) + "2" + window.location.hash.substr(10, 2);
			//});
			//$("body").on("click", ".prototype-publishDraftButton", function () {
			//    window.location.hash = window.location.hash.substr(0, 9) + "00" + window.location.hash.substr(11, 1);
			//});
			//$("body").on("click", ".prototype-closeDraftButton", function () {
			//    window.location.hash = window.location.hash.substr(0, 9) + "0" + window.location.hash.substr(10, 2);
			//});
			//$("body").on("click", ".prototype-uploadButton", function () {
			//    $(".prototype-uploadTab a").click();
			//});
			//$("body").on("change", ".prototype-dateSelect", function () {
			//    window.location.hash = window.location.hash.substr(0, 9) + "1" + window.location.hash.substr(10, 2);
			//});
			//$(".prototype-dateSelect").on("change", function () {
			//    window.location.hash = window.location.hash.substr(0, 9) + "1" + window.location.hash.substr(10, 2);
			//});

			//$(".r360-psSectionSelect").on("change", function(e) {
            //    $(this).closest(".panel-heading").find(".panel-title").text($(this).find(".multiselect-selected-text").text());
			//}).closest(".panel-heading").find(".panel-title").text($(".r360-psSectionSelect .multiselect-selected-text").text());			

			//$(".r360-psEventsLeadingLaggingSelect").on("change", function(e) {
            //    $(this).closest(".panel-heading").find(".panel-title").text($(this).find(".multiselect-selected-text").text());
			//}).closest(".panel-heading").find(".panel-title").text($(".r360-psEventsLeadingLaggingSelect .multiselect-selected-text").text());			
			
		},

		getPanels: function (hash, callback) {
		    this.analyticsTrinityViewChartViewOptions = {
		        yAxis: {
		            gridLineColor: "#CCD4EF",
		            endOnTick: false
		        },
		        xAxis: {
		            type: 'datetime'
		        },
		        chart: { height: 150 },
		        plotOptions: { line: { connectNulls: true }, tooltip: { hideDelay: 0 } }
		    };
		    if (hash == '#TRINITY_TEST') {
		        callback(radient360.module.r360.trinityTestPanel());
		    } else if (hash == '#UNNAMED04_TEST') {
		        callback(radient360.module.r360.unnamed04TestPanel());
		    } else if (hash == '#PS') {
		        callback(radient360.module.r360.temporaryPsPanels());
			} else {
			    callback();
			}
		},

		trinityTestPanel: function () {
		    return {
		        type: "groupPanel",
		        size: "12",
		        options: {
		            title: "Trinity Test",
		            tabbed: true,
		            contents: [{
		                type: "basicPanel",
		                size: 12,
		                options: {
		                    title: "Trinity Test",
		                    contents: [{
		                        type: "analyticsTrinityView",
		                        options: {
		                            tickPositioner: { minOffset: 80 },
		                            colors: ["#74C4FD", '#1BDD4E', 'red'],
		                            model: {
		                                labels: null,
		                                seriesKind: 'Type',
		                                series: [{
		                                    name: "% Training - SeaRose",
		                                    data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                    [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                    [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
			                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                    [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                    [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
		                                    marker: {
		                                        symbol: 'diamond',
		                                        fillColor: '#FFFFFF',
		                                        lineWidth: 2,
		                                        lineColor: null
		                                    },
		                                    zIndex: 3,
		                                    details: { current: [1], ytd: [2], target: [3], type: [4] }
		                                }, {
		                                    name: "Target",
		                                    data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                        [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                        [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                        [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                        [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                        [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
		                                    dashStyle: 'Dash',
		                                    tooltip: { enabled: false },
		                                    enableMouseTracking: false,
		                                    allowPointSelect: false,
		                                    zIndex: 2,
		                                    marker: {
		                                        enabled: false,
		                                        states: { hover: { enabled: false }, select: { enabled: false } }
		                                    }
		                                }, {
		                                    name: "Incident Report",
		                                    type: 'column',
		                                    marker: { enabled: false },
		                                    allowPointSelect: false,
		                                    point: {
		                                        events: {
		                                            click: null,
		                                            select: null,
		                                            unselect: null
		                                        }
		                                    },
		                                    zIndex: 1,
		                                    tooltip: {
		                                        pointFormatter: function (e) {
		                                            return this.series.userOptions.details[this.index];
		                                        }
		                                    },
		                                    data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
			                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
		                                    details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
			                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                            'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
			                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
		                                }]
		                            },
		                            chartView: {
		                                type: "lineChartView",
		                                options: this.analyticsTrinityViewChartViewOptions		                                
		                            },
		                            legendView: {
		                                type: 'kpiExtendedLineLegendView',
		                                options: { line: { chart: { height: 25 } } }
		                            },
		                            detailsView: {
		                                type: 'psInfoCardDetailsView'
		                            }
		                        }
		                    }]
		                }
		            }]
		        }
		    };
		},

		unnamed04TestPanel: function () {
		    return {
		        type: "groupPanel",
		        size: "12",
		        options: {
		            title: "Unnamed04 Test",
		            tabbed: true,
		            contents: [{
		                type: "basicPanel",
		                size: 12,
		                options: {
		                    maxHeight: "500px",
		                    title: "Unnamed04 Test",
		                    contents: {
		                        type: "unnamed04",
		                        options: {
		                            data: [
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-arrow-up status-good",
                                                text: "There were five ESD 5's and one ESD 2 on SeaRose in September. The ESD 2 had an associated deluge release as a result of a repeated incident. See Section 20 for details.",
                                                elements: "#3, #4, #5",
                                                section: "SeaRose Blocks, Alarms and Faults"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-ok",
                                                text: "Both active alarms and ESD blocks peaked on September 27th as a result of the aforementioned ESD 2. There was a smaller peak on September 21st due to subsea project work.",
                                                elements: "#3, #4, #5",
                                                section: "SeaRose Blocks, Alarms and Faults"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        colors: ["#74C4FD", 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "SeaRose TRIR Rolling",
                                                                data: [[Date.UTC(2011, 9, 31), 0.54], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 0], [Date.UTC(2012, 0, 31), 0],
                                                                        [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0.53], [Date.UTC(2012, 3, 30), 0.53], [Date.UTC(2012, 4, 31), 0.53],
                                                                        [Date.UTC(2012, 5, 30), 0.51], [Date.UTC(2012, 6, 31), 0.47], [Date.UTC(2012, 7, 31), 0.93], [Date.UTC(2012, 8, 30), 1.4],
                                                                        [Date.UTC(2012, 9, 31), 1.4], [Date.UTC(2012, 10, 30), 1.4], [Date.UTC(2012, 11, 31), 1.39], [Date.UTC(2013, 0, 31), 1.4],
                                                                        [Date.UTC(2013, 1, 28), 1.4], [Date.UTC(2013, 2, 31), 1.4], [Date.UTC(2013, 3, 30), 1.41], [Date.UTC(2013, 4, 31), 1.41],
                                                                        [Date.UTC(2013, 5, 30), 1.45], [Date.UTC(2013, 6, 31), 1.6], [Date.UTC(2013, 7, 31), 1.6], [Date.UTC(2013, 8, 30), 1.07]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 2], [Date.UTC(2011, 10, 2), 2], [Date.UTC(2011, 10, 7), 2], [Date.UTC(2012, 4, 30), 2],
                                                                [Date.UTC(2012, 6, 15), 2], [Date.UTC(2012, 7, 2), 2], [Date.UTC(2012, 9, 21), 2], [Date.UTC(2012, 10, 7), 2]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }, {
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        colors: ["#74C4FD", '#d8ddf0', '#0C8EEA', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "Tier 1 PSE - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 0], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 0], [Date.UTC(2012, 0, 31), 0],
                                                                        [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0], [Date.UTC(2012, 3, 30), 0], [Date.UTC(2012, 4, 31), 0],
                                                                        [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 0], [Date.UTC(2012, 8, 30), 1],
                                                                        [Date.UTC(2012, 9, 31), 0], [Date.UTC(2012, 10, 30), 0], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 0],
                                                                        [Date.UTC(2013, 1, 28), 0], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 0],
                                                                        [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] },
                                                                kpi: { status: 'non-critical' }
                                                            }, {
                                                                name: "Tier 2 PSE - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 0], [Date.UTC(2011, 10, 30), 1], [Date.UTC(2011, 11, 31), 3], [Date.UTC(2012, 0, 31), 0],
                                                                        [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 0],
                                                                        [Date.UTC(2012, 5, 30), 1], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 0], [Date.UTC(2012, 8, 30), 0],
                                                                        [Date.UTC(2012, 9, 31), 0], [Date.UTC(2012, 10, 30), 1], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 1],
                                                                        [Date.UTC(2013, 1, 28), 1], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 0],
                                                                        [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] },
                                                                kpi: { status: 'non-critical' }
                                                            }, {
                                                                name: "Tier 3 PSE - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 1], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 1], [Date.UTC(2012, 0, 31), 1],
                                                                        [Date.UTC(2012, 1, 28), 1], [Date.UTC(2012, 2, 31), 1], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 0],
                                                                        [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 3], [Date.UTC(2012, 8, 30), 4],
                                                                        [Date.UTC(2012, 9, 31), 3], [Date.UTC(2012, 10, 30), 2], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 3],
                                                                        [Date.UTC(2013, 1, 28), 0], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 2],
                                                                        [Date.UTC(2013, 5, 30), 1], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
                                                                marker: {
                                                                    symbol: 'square',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] },
                                                                kpi: { status: 'critical' }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                kpi: { status: null },
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 5], [Date.UTC(2011, 10, 2), 5], [Date.UTC(2011, 10, 7), 5], [Date.UTC(2012, 4, 30), 5],
                                                                [Date.UTC(2012, 6, 15), 5], [Date.UTC(2012, 7, 2), 5], [Date.UTC(2012, 9, 21), 5], [Date.UTC(2012, 10, 7), 5]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                            //options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {
                                                            //    xAxis: {
                                                            //        plotLines: [{ // mark the weekend
                                                            //            color: 'red',
                                                            //            details: "IR-12-14862: Dropped Object from pipe barn crane (HiPo NM)",
                                                            //            width: 2,
                                                            //            value: Date.UTC(2014, 6, 1),
                                                            //            events: {
                                                            //                click: function (e) {

                                                            //                },
                                                            //                mouseover: function () {

                                                            //                },
                                                            //                mouseout: function () {

                                                            //                }
                                                            //            }
                                                            //        }]
                                                            //    },
                                                            //})
                                                            //options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {			                               
                                                            //    plotOptions: {
                                                            //        column: {
                                                            //            borderWidth: 0,
                                                            //            shadow: false,
                                                            //            pointWidth: 2,
                                                            //            stacking: null,
                                                            //            color: 'red'
                                                            //        }
                                                            //    }
                                                            //}),
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }, {
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        tickPositioner: { minOffset: 80 },
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            //options: this.analyticsTrinityViewChartViewOptions
                                                            options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {
                                                                yAxis: {
                                                                    breaks: [{
                                                                        from: 1,
                                                                        to: 80,
                                                                        breakSize: 0
                                                                    }]
                                                                }
                                                            }),
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }, {
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        colors: ['#0C8EEA', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "Monthly SR Trips",
                                                                data: [[Date.UTC(2011, 9, 31), 7], [Date.UTC(2011, 10, 30), 2], [Date.UTC(2011, 11, 31), 5], [Date.UTC(2012, 0, 31), 5],
                                                                        [Date.UTC(2012, 1, 28), 12], [Date.UTC(2012, 2, 31), 4], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 2],
                                                                        [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), 12],
                                                                        [Date.UTC(2012, 9, 31), 5], [Date.UTC(2012, 10, 30), 8], [Date.UTC(2012, 11, 31), 3], [Date.UTC(2013, 0, 31), 5],
                                                                        [Date.UTC(2013, 1, 28), 3], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 4], [Date.UTC(2013, 4, 31), 1],
                                                                        [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 2], [Date.UTC(2013, 7, 31), 1], [Date.UTC(2013, 8, 30), 3]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 14], [Date.UTC(2011, 10, 2), 14], [Date.UTC(2011, 10, 7), 14], [Date.UTC(2012, 4, 30), 14],
                                                                [Date.UTC(2012, 6, 15), 14], [Date.UTC(2012, 7, 2), 14], [Date.UTC(2012, 9, 21), 14], [Date.UTC(2012, 10, 7), 14]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-exclamation-sign status-non-critical",
                                                text: "Near miss events have increased by two from August to September. " +
                                                        "The process safety related near miss with the highest potential severity (IR-14-18014) was the failure of a 3/4\" downline hose during a pressure test. " +
                                                        "This took place on the Atlantic Osprey.",
                                                elements: "#3, #4, #5",
                                                section: "Near Miss Events"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-arrow-down status-critical",
                                                text: "There was one Isolation/Permit to Work event for SeaRose, IR-14-17975 (still under investigation). " +
                                                        "There was one Isolation/Permit to Work event for SeaRose, IR-14-17975 (still under investigation). There were also two audits, a STARS Card and a non-conformance report (NCR-14-0253) with Permit to Work findings. " +
                                                        "The audit results found controls on an SJA that were not signed off, and a PSIC number that was included but never used with the permit. " +
                                                        "A September 11th STARS Card indicated that site visits on permits are not being consistently completed by both area authority and performing authority together. " +
                                                        "NCR-14-0253 was raised during the MPG B controls upgrade work; two workscopes were being completed simultaneously and a PSIC was removed without the knowledge of all personnel involved.",
                                                elements: "#3, #4, #5",
                                                section: "Permission to Work Isolation Events"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-arrow-up status-good",
                                                text: "There was one observation (OBS-14-17493), and one STARS Card highlighting both P&ID legibility and management of change as an issue.",
                                                elements: "#3, #4, #5",
                                                section: "NOPRAS"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-ok",
                                                text: "Overall there were 5 process safety STARS cards in September.",
                                                elements: "#3, #4, #5",
                                                section: "Timely Investigation of Serious+ Incidents"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-exclamation-sign status-non-critical",
                                                text: "P&ID Quality (as part of HOIMS Element 9) was the focus of the September Process Safety Reminder; see AR-HSE-SR-0022.",
                                                elements: "#9",
                                                section: "SeaRose Trips"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-arrow-down status-critical",
                                                text: "Note that there has been no update for 'Timely Preventative Maintenance Work Order Completion' since June 20th due to reporting issues since the implementation of Vanilla Sky.",
                                                elements: "#5",
                                                section: "Occupational Incident Rate (TRIR)"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training - SeaRose",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Target",
                                                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                                            [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                                            [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                                            [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                                            [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                                            [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
                                                                dashStyle: 'Dash',
                                                                tooltip: { enabled: false },
                                                                enableMouseTracking: false,
                                                                allowPointSelect: false,
                                                                zIndex: 2,
                                                                marker: {
                                                                    enabled: false,
                                                                    states: { hover: { enabled: false }, select: { enabled: false } }
                                                                }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 100], [Date.UTC(2011, 10, 2), 100], [Date.UTC(2011, 10, 7), 100], [Date.UTC(2012, 4, 30), 100],
                                                                [Date.UTC(2012, 6, 15), 100], [Date.UTC(2012, 7, 2), 100], [Date.UTC(2012, 9, 21), 100], [Date.UTC(2012, 10, 7), 100]],
                                                                details: ['IR-11-17114: Potential fall from height (HiPo NM)', 'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                                                'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)', 'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                                                'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System', 'IR-12-15582: Fire in Aft Fire Pump Room',
                                                                'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA', 'IR-12-18011: Missing deck plate between decks']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    }]
		                        }
		                    }
		                }
		            }]
		        }
		    };
		},

		temporaryPsPanels: function (flags) {
			var mediumHeightOptions = { maxHeight: '400px' };
			var largeHeightOptions = { maxHeight: '600px' };			
			var that = this;
			var $modalDiv = $('<div class="tempModalDiv"></div>');
			$modalDiv.appendTo(radient360.page.$contents);
			$modalDiv.modal({ classes: "col-sm-8", showCloseBtn: false, enableEscapeKey: false });
			var editKnownIssueSelections = [{
			    type: "analyticsTrinityView",
			    options: {
			        colors: ["#74C4FD", '#1BDD4E', '#0C8EEA'],
			        model: {
			            labels: null,
			            seriesKind: 'Type',
			            series: [{
			                name: "% Overdue Docs",
			                data: [[Date.UTC(2013, 9, 31), 5.31], [Date.UTC(2013, 10, 30), 6.78], [Date.UTC(2013, 11, 31), 6.14], [Date.UTC(2014, 0, 31), 8.10],
                               [Date.UTC(2014, 1, 28), 8.21], [Date.UTC(2014, 2, 31), 6.13], [Date.UTC(2014, 3, 30), 7.22], [Date.UTC(2014, 4, 31), 7.72],
                               [Date.UTC(2014, 5, 30), 7.83], [Date.UTC(2014, 6, 31), 7.94], [Date.UTC(2014, 7, 31), 10.21], [Date.UTC(2014, 8, 30), 6.24]],
			                marker: {
			                    symbol: 'square',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: { status: 'critical' }
			            }, {
			                name: "% Overdue Target",
			                data: [[Date.UTC(2013, 9, 31), 5], [Date.UTC(2013, 10, 30), null], [Date.UTC(2013, 11, 31), null], [Date.UTC(2014, 0, 31), null],
                                [Date.UTC(2014, 1, 28), null], [Date.UTC(2014, 2, 31), null], [Date.UTC(2014, 3, 30), null], [Date.UTC(2014, 4, 31), null],
                                [Date.UTC(2014, 5, 30), null], [Date.UTC(2014, 6, 31), null], [Date.UTC(2014, 7, 31), null], [Date.UTC(2014, 8, 30), 5]],
			                dashStyle: 'Dash',
			                tooltip: { enabled: false },
			                enableMouseTracking: false,
			                allowPointSelect: false,
			                marker: {
			                    enabled: false,
			                    states: { hover: { enabled: false }, select: { enabled: false } }
			                },
			                kpi: { status: null }
			            }, {
			                name: "% Criticality 1 Overdue",
			                data: [[Date.UTC(2013, 9, 31), 1.11], [Date.UTC(2013, 10, 30), 1.48], [Date.UTC(2013, 11, 31), 0.98], [Date.UTC(2014, 0, 31), 1.60],
                                [Date.UTC(2014, 1, 28), 1.35], [Date.UTC(2014, 2, 31), 0.74], [Date.UTC(2014, 3, 30), 0.98], [Date.UTC(2014, 4, 31), 1.10],
                                [Date.UTC(2014, 5, 30), 1.22], [Date.UTC(2014, 6, 31), 1.10], [Date.UTC(2014, 7, 31), 0.98], [Date.UTC(2014, 8, 30), 0.50]],
			                marker: {
			                    symbol: 'diamond',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: { status: 'non-critical' }
			            }]
			        },
			        chartView: {
			            type: "lineChartView",
			            options: this.analyticsTrinityViewChartViewOptions
			        },
			        legendView: {
			            type: 'kpiExtendedLineLegendView',
			            options: { line: { chart: { height: 25 } } }
			        },
			        detailsView: {
			            type: 'psInfoCardDetailsView'
			        }
			    }
			}, {
			    type: "analyticsTrinityView",
			    options: {
			        colors: ['#0C8EEA', "#74C4FD", "#2f89cd"],
			        model: {
			            labels: null,
			            seriesKind: 'Type',
			            series: [{
			                name: "All Documents",
			                data: [[Date.UTC(2013, 9, 31), 810], [Date.UTC(2013, 10, 30), 811], [Date.UTC(2013, 11, 31), 814], [Date.UTC(2014, 0, 31), 815],
                                [Date.UTC(2014, 1, 28), 816], [Date.UTC(2014, 2, 31), 816], [Date.UTC(2014, 3, 30), 817], [Date.UTC(2014, 4, 31), 816],
                                [Date.UTC(2014, 5, 30), 817], [Date.UTC(2014, 6, 31), 819], [Date.UTC(2014, 7, 31), 813], [Date.UTC(2014, 8, 30), 801]],
			                marker: {
			                    symbol: 'diamond',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: null
			            }, {
			                name: "Total Documents Overdue",
			                data: [[Date.UTC(2013, 9, 31), 43], [Date.UTC(2013, 10, 30), 50], [Date.UTC(2013, 11, 31), 55], [Date.UTC(2014, 0, 31), 66],
                                [Date.UTC(2014, 1, 28), 67], [Date.UTC(2014, 2, 31), 50], [Date.UTC(2014, 3, 30), 59], [Date.UTC(2014, 4, 31), 63],
                                [Date.UTC(2014, 5, 30), 64], [Date.UTC(2014, 6, 31), 65], [Date.UTC(2014, 7, 31), 83], [Date.UTC(2014, 8, 30), 50]],
			                marker: {
			                    symbol: 'square',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: null
			            }]
			        },
			        chartView: {
			            type: "lineChartView",
			            options: this.analyticsTrinityViewChartViewOptions
			        },
			        legendView: {
			            type: 'kpiExtendedLineLegendView',
			            options: { line: { chart: { height: 25 } } }
			        },
			        detailsView: {
			            type: 'psInfoCardDetailsView'
			        }
			    }
			}, {
			    type: "analyticsTrinityView",
			    options: {
			        colors: ['#0C8EEA', "#74C4FD", "#d8ddf0"],
			        model: {
			            labels: null,
			            seriesKind: 'Type',
			            series: [{
			                name: "Overdue Criticality 1 Documents",
			                data: [[Date.UTC(2013, 9, 31), 9], [Date.UTC(2013, 10, 30), 12], [Date.UTC(2013, 11, 31), 8], [Date.UTC(2014, 0, 31), 13],
                                [Date.UTC(2014, 1, 28), 11], [Date.UTC(2014, 2, 31), 6], [Date.UTC(2014, 3, 30), 8], [Date.UTC(2014, 4, 31), 9],
                                [Date.UTC(2014, 5, 30), 10], [Date.UTC(2014, 6, 31), 9], [Date.UTC(2014, 7, 31), 8], [Date.UTC(2014, 8, 30), 4]],
			                marker: {
			                    symbol: 'diamond',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: null
			            }, {
			                name: "Overdue Criticality 2 Documents",
			                data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 22], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 29],
                                [Date.UTC(2014, 1, 28), 31], [Date.UTC(2014, 2, 31), 24], [Date.UTC(2014, 3, 30), 26], [Date.UTC(2014, 4, 31), 29],
                                [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 31], [Date.UTC(2014, 7, 31), 46], [Date.UTC(2014, 8, 30), 25]],
			                marker: {
			                    symbol: 'square',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: null
			            }, {
			                name: "Overdue Criticality 3 Documents",
			                data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 21], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 24],
                                [Date.UTC(2014, 1, 28), 25], [Date.UTC(2014, 2, 31), 20], [Date.UTC(2014, 3, 30), 25], [Date.UTC(2014, 4, 31), 25],
                                [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 25], [Date.UTC(2014, 7, 31), 29], [Date.UTC(2014, 8, 30), 21]],
			                marker: {
			                    symbol: 'triangle',
			                    fillColor: '#FFFFFF',
			                    lineWidth: 2,
			                    lineColor: null
			                },
			                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                kpi: null
			            }]
			        },
			        chartView: {
			            type: "lineChartView",
			            options: this.analyticsTrinityViewChartViewOptions
			        },
			        legendView: {
			            type: 'kpiExtendedLineLegendView',
			            options: { line: { chart: { height: 25 } } }
			        },
			        detailsView: {
			            type: 'psInfoCardDetailsView'
			        }
			    }
			}];           

			var refreshEditKnownIssueLinkPanel = function (selectedValues) {
			    radient360.page.callJQueryUiMethod($(".r360Panel-editKnownIssueLinkPanel"), "refresh", []);
			    //$(".r360Panel-editKnownIssueLinkPanel").resize();
			};
			
			var onProcessSafetyButtonClick = function (e, buttondata) {
			    $(".r360Panel-processSafetyEvents .radient360View-pieChartView")['pieChartView']("option", "seriesIndex", buttondata.index)
			    $(".r360Panel-processSafetyEvents .radient360View-singleSeriesLegendView")['singleSeriesLegendView']("option", "seriesIndex", buttondata.index)
			};

			var onEditKnownIssueSelectChange = function (e, selectedValues) {
			    if (that.asdfTimer) {
			        clearTimeout(that.asdfTimer);
			    }
			    that.asdfTimer = setTimeout(function () {
			        var selections = [];
			        if (selectedValues.values) {
			            for (var i = 0; i < selectedValues.values.length; ++i) {
			                selections.push(editKnownIssueSelections[i % editKnownIssueSelections.length]);
			            }
			        }
			        $(".r360Panel-editKnownIssueLinkPanel .radient360Widget-widgetGroup")['widgetGroup']("option", "contents", selections);
			        refreshEditKnownIssueLinkPanel();
			    }, 5);

			};
			var onReportSummarySelectionChange = function (e, selectedValues) {
			    var section = that.reportSummaryWidgets[selectedValues.values];
			    if (!section) {
			        section = that.reportSummaryWidgets['No Data'];
			    }
			    if (section) {
			        $(".r360-psReportSummaryPanel")['basicPanel']("option", "contents", section);
			    }
			};
			var onReportSummaryEditNotableTrendClick = function (section, text, selectedIconIndex) {
			    return function () {
			        $(".tempModalDiv")['modal']("option", "panel", {
			            type: 'editNotableTrendFormPanel', options: {
			                title: "Edit Notable Trend: " + section,
			                iconSelection: {
			                    initialSelectedItemIndex: selectedIconIndex,
			                    icons: [{ iconClassString: "glyphicon glyphicon-ok" }, { iconClassString: "glyphicon glyphicon-arrow-up status-good" },
                                    { iconClassString: "glyphicon glyphicon-arrow-down status-critical" }, { iconClassString: "glyphicon glyphicon-exclamation-sign status-non-critical" }]
			                },
			                data: {
			                    text: text
			                }
			            }
			        });
			    }
			};
			this.reportSummaryWidgets = {
			    'No Data': [/*{
			        type: 'noModel',
			        options: {
			            text: "No Data for this Month"
			        }
			    }*/],
			    'Occupational Incident Rate': [{
			        type: 'widgetGroup',
			        options: {
			            contents: [{
			                type: 'unnamed02',
			                options: {
			                    metadata: {
			                        attributes: [{ name: 'kpiType', title: 'KPI Type' }, { name: 'kpiDef', title: 'KPI Definition' },
                                        { name: 'defSource', title: 'Definition Source' }, { name: 'gaps', title: 'Gaps' }]
			                    }, data: {
			                        kpiType: 'Lagging', kpiDef: 'Total Recordable Occupational Incident Rate',
			                        defSource: 'Occupational Scorecard Incident',
			                        gaps: 'N/A'
			                    }
			                }
			            }, {
			                type: 'kpiEditableInfoCard', options: {
			                    title: 'Notable Trend :', contents: "Occupational Incident Rate declining.",
			                    kpi: { contents: 'glyphicon glyphicon-arrow-up status-good' },
			                    onEdit: onReportSummaryEditNotableTrendClick("Occupational Incident Rate", "Declining", 1)
			                }
			            }]
			        }
			    }, {
			        type: 'noModel',
			        options: {
			            text: "No Data for this Month"
			        }
			    }],
			    'Documents': [{
			        type: 'widgetGroup',
			        options: {
			            contents: [{
			                type: 'unnamed02',
			                options: {
			                    metadata: {
			                        attributes: [{ name: 'kpiType', title: 'KPI Type' }, { name: 'kpiDef', title: 'KPI Definition' },
                                        { name: 'defSource', title: 'Definition Source' }, { name: 'gaps', title: 'Gaps' }]
			                    }, data: {
			                        kpiType: 'Leading', kpiDef: 'Percentage of overdue documents by Criticality.',
			                        defSource: 'N/A', gaps: 'N/A'
			                    }
			                }
			            }, {
			                type: 'kpiEditableInfoCard', options: {
			                    title: 'Notable Trend :', contents: "Document 4748-JSHF-7473-7473 has been overdue for the past 2 months.",
			                    kpi: { contents: 'glyphicon glyphicon-arrow-down status-critical' },
			                    onEdit: onReportSummaryEditNotableTrendClick("Documents",
                                    "Document 4748-JSHF-7473-7473 has been overdue for the past 2 months.",
                                    2)
			                }
			            }]
			        }
			    }, {
			        type: "columnChartView",
			        options: {
			            chart: { height: 450 },
			            legend: { enabled: true },
			            plotOptions: {
			                series: {
			                    dataLabels: {
			                        color: '#6E7A97'
			                    }
			                },
			                column: {
			                    stacking: null
			                },
			                line: {
			                    showInLegend: false,
			                    lineWidth: 0,
			                    dataLabels: {
			                        shape: 'circle',
			                        backgroundColor: '#FFFFFF',
			                        padding: 5,
			                        enabled: true,
			                        crop: false,
			                        overflow: "none",
			                        allowOverlap: true,
			                        verticalAlign: 'middle',
			                        formatter: function () {
			                            return radient360.utility.charting.percentDataLabelFormatter(this, 1, 0)
			                        },
			                        y: 7
			                    }
			                }
			            },
			            xAxis: {
			                labels: { y: 25 }
			            },
			            yAxis: {
			                tickInterval: 10,
			                lineColor: '#000000',
			                lineWidth: 1,
			                breaks: [{
			                    from: 100,
			                    to: 465,
			                    breakSize: 0
			                }]
			            },
			            title: {
			                text: "6 % Overdue for Review (50 of 801 documents overdue)",
			                style: {
			                    color: '#6E7A97',
			                    fontWeight: 600,
			                    fontFamily: "'Open Sans', sans-serif"
			                }
			            },
			            colors: ['#1573b9', "#74C4FD"],
			            model: {
			                labels: ['Admin Department', 'Production Department', 'Finance Department'],
			                seriesKind: 'Type',
			                series: [{
			                    name: "Total Documents",
			                    data: [16, 27, 33]
			                }, {
			                    name: "Overdue for Review",
			                    data: [0, 2, 3]
			                }, {
			                    type: 'line',
			                    name: "% Overdue",
			                    data: [0, 0, 0],
			                    tooltip: { enabled: false },
			                    enableMouseTracking: false,
			                    allowPointSelect: false,
			                    marker: {
			                        enabled: false,
			                        states: { hover: { enabled: false }, select: { enabled: false } }
			                    }
			                }]
			            }
			        }
			    }, {
			        type: "table",
			        options: {
			            caption: 'Overdue Documents by Department and Criticality',
			            data: {
			                header: {
			                    rows: [{
			                        cells: [
                                        {
                                            type: "labelCell", options: {
                                                title: "Department"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "% Overdue"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "Criticality 1"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "Criticality 2"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "Criticality 3"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "Total"
                                            }
                                        }]
			                    }]
			                },
			                body: {
			                    rows: [{
			                        cells: [
                                        {
                                            type: "labelCell", options: {
                                                title: "Admin Department"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0%"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        }]
			                    }, {
			                        cells: [
                                        {
                                            type: "labelCell", options: {
                                                title: "Production Department"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "7%"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "1"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "1"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "2"
                                            }
                                        }]
			                    }, {
			                        cells: [
                                        {
                                            type: "labelCell", options: {
                                                title: "Finance Department"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "9%"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "3"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "0"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "3"
                                            }
                                        }]
			                    },
                                {
			                        cells: [
                                        {
                                            type: "labelCell", options: {
                                                title: "Total",
                                                classes: 'total-label'
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "6%"
                                            }
                                        },
                                        {
                                            type: "controlIconNavigate", options: {
                                                modal: { enabled: true },
                                                number: 4,
                                                //iconClassString: "glyphicon glyphicon-new-window",
                                                panel: {
                                                    type: 'tablePanel', options: {
                                                        title: "Criticality 1 Overdue Documents",
                                                        dataUrl: null,
                                                        data: {
                                                            header: {
                                                                rows: [{
                                                                    cells: [
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Department"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Title"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Document Number"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Rev."
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Expiry"
                                                                            }
                                                                        }
                                                                    ]
                                                                }]
                                                            },
                                                            body: {
                                                                rows: [{
                                                                    cssClass: 'active',
                                                                    cells: [
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Admin Department"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Administration Strategy"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "8347-KDHC-4373-9473"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "R2"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "1/1/2014"
                                                                            }
                                                                        }
                                                                    ]
                                                                }, {
                                                                    cells: [
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Finance Department"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "Finance Guide"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "3838-JDHD-8473-9371"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "R3"
                                                                            }
                                                                        },
                                                                        {
                                                                            type: "labelCell", options: {
                                                                                title: "7/7/2014"
                                                                            }
                                                                        }
                                                                    ]
                                                                }]
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "25"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "21"
                                            }
                                        },
                                        {
                                            type: "labelCell", options: {
                                                title: "50"
                                            }
                                        }]
			                    }]
			                },
			            }
			        }
			    }]
			};
			var tabControls = [];

			if (!(flags & 0x0200)) {
				tabControls.push({
					type: 'controlSelect',
					options: {
						items: ["February, 2015",
						    "January, 2015", 
						    "December, 2014",
                            "November, 2014",
                            "October, 2014",
                            "September, 2014"],
						label: 'Report Date:',
                        width: 145,
						initialSelectedItemIndex: flags & 0x0100 ? 2 : 0,
						classes: "r360-psTabSelect prototype-dateSelect"
					}
				});
			}

			if (flags & 0x004) {
				tabControls.push({
					type: "controlIcon",
					options: {
						iconClassString: 'fa fa-file-word-o'
					}
				});
			}

			if (flags & 0x001 && !(flags & 0x200)) {
			    tabControls.push({
			        type: "controlIcon",
			        options: {
			            iconClassString: 'glyphicon glyphicon-cloud-upload',
			            classes: 'prototype-uploadButton'
			        }
			    });
            }
			var draft = flags & 0x0200;

			if (flags & 0x002) {
				if (flags & 0x0200) {
					tabControls.push({
						type: "button",
						options: {
						    text: "Close Draft",
						    iconClass: "glyphicon glyphicon-remove",
							confirmRequired: false,
							classes: 'prototype-closeDraftButton'
						}
					});
					tabControls.push({
						type: "button",
						options: {
							text: "Publish",
						    //iconClass: "glyphicon glyphicon-ok",
							disabled: (flags & 0x0020 ? false : (flags & 0x0010 ? true : true)),
							buttonType: "primary",
							confirmRequired: true,
							classes: 'prototype-publishDraftButton'
						}
					});
				} else {
					tabControls.push({
						type: "button",
						options: {
							//buttonType: (flags & 0x0020 ? false : (flags & 0x0010 ? true : true)) ? 'default' : 'success',
							text: 'Draft',
							iconClass: 'glyphicon glyphicon-pencil',
							//disabled: (flags & 0x0020 ? false : (flags & 0x0010 ? true : true)),
							classes: 'prototype-draftButton'
						}
					});
				}
			}
			var overdueTabControls = tabControls.slice(0);
			overdueTabControls.push(
			    {
			        type: "tooltip",
			        options: {
			            classes: 'fa fa-info-circle info-icon',
			            contents: "Section #9",
			            customOptions: { placement: 'left' }
			        }
			    }
            );

			return {
			    type: "groupPanel",
			    size: 12,
			    options: {
			        title: "Home",
			        tabbed: true,
			        userCloseable: false,
			        controlWidget: {
			            type: "controlBar",
			            options: {
			                controls: tabControls
			            }
			        },
			        contents: [{
			            type: "basicPanel",
			            size: 12,
			            options: {
			                title: "Key Performance",
			                classes: 'r360Panel-psHoimsKeyPerformance collapsed',
			                renderPlayControls: true,
			                contents: [{
			                    type: 'widgetGroup',
			                    options: {
			                        classes: 'blockBar blockBar-6 collapsed',
			                        contents: [
                                    {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0,                                 
                                                breaks: [{
                                                    from: 3,
                                                    to: 10,
                                                    breakSize: 0
                                                }]
                                            },
                                            model: {
                                                labels: ["Element #2"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#1BDD4E",
                                                    name: "Good",
                                                    data: [3]
                                                }, {
                                                    color: "#FFBA00",
                                                    name: "Non-Critical",
                                                    data: [1]
                                                }, {
                                                    color: "#9299B0",
                                                    name: "Critical",
                                                    data: [0]
                                                }, {
                                                    color: "#d8ddf0",
                                                    name: "Unknown",
                                                    data: [11]
                                                }]
                                            }
                                        }
                                    }, {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0,
                                                breaks: [{
                                                    from: 1,
                                                    to: 7,
                                                    breakSize: 0
                                                }]
                                            },
                                            model: {
                                                labels: ["Element #5"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#1BDD4E",
                                                    name: "Good",
                                                    data: [1]
                                                }, {
                                                    color: "#9299B0",
                                                    name: "Non-Critical",
                                                    data: [0]
                                                }, {
                                                    color: "#FF5A00",
                                                    name: "Critical",
                                                    data: [1]
                                                }, {
                                                    color: "#d8ddf0",
                                                    name: "Unknown",
                                                    data: [8]
                                                }]
                                            }
                                        }
                                    }, {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0,
                                                breaks: [{
                                                    from: 4,
                                                    to: 14,
                                                    breakSize: 0
                                                }]
                                            },
                                            model: {
                                                labels: ["Element #6"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#9299B0",
                                                    name: "Good",
                                                    data: [0]
                                                }, {
                                                    color: "#FFBA00",
                                                    name: "Non-Critical",
                                                    data: [3]
                                                }, {
                                                    color: "#FF5A00",
                                                    name: "Critical",
                                                    data: [4]
                                                }, {
                                                    color: "#d8ddf0",
                                                    name: "Unknown",
                                                    data: [15]
                                                }]
                                            }
                                        }
                                    }, {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0
                                            },
                                            model: {
                                                labels: ["Element #7"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#1BDD4E",
                                                    name: "Good",
                                                    data: [1]
                                                }, {
                                                    color: "#FFBA00",
                                                    name: "Non-Critical",
                                                    data: [4]
                                                }, {
                                                    color: "#FF5A00",
                                                    name: "Critical",
                                                    data: [2]
                                                }, {
                                                    color: "#9299B0",
                                                    name: "Unknown",
                                                    data: [0]
                                                }]
                                            }
                                        }
                                    }, {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0
                                            },
                                            model: {
                                                labels: ["Element #8"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#1BDD4E",
                                                    name: "Good",
                                                    data: [4]
                                                }, {
                                                    color: "#9299B0",
                                                    name: "Non-Critical",
                                                    data: [0]
                                                }, {
                                                    color: "#FF5A00",
                                                    name: "Critical",
                                                    data: [2]
                                                }, {
                                                    color: "#d8ddf0",
                                                    name: "Unknown",
                                                    data: [5]
                                                }]
                                            }
                                        }
                                    }, {
                                        type: "columnChartView",
                                        options: {
                                            chart: {
                                                height: 110,
                                                marginBottom: 20
                                            },
                                            plotOptions: {
                                                column: { stacking: null, minPointLength: 2 },
                                                series: {
                                                    dataLabels: { style: { "color": '#6e7a97', "fontSize": "11px", "fontWeight": "600", "textShadow": "none" }, verticalAlign: 'bottom', inside: true, y: 23, crop: false, overflow: 'none' }
                                                }
                                            },
                                            xAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                lineWidth: 0,
                                                gridLineWidth: 0,
                                                tickWidth: 0,
                                                tickLength: 0
                                            },
                                            yAxis: {
                                                endOnTick: false,
                                                startOnTick: false,
                                                labels: {
                                                    enabled: false
                                                },
                                                title: {
                                                    text: null
                                                },
                                                gridLineWidth: 0,
                                                breaks: [{
                                                    from: 4,
                                                    to: 20,
                                                    breakSize: 0
                                                }]
                                            },
                                            model: {
                                                labels: ["Element #9"],
                                                seriesKind: 'Status',
                                                series: [{
                                                    color: "#9299B0",
                                                    name: "Good",
                                                    data: [0]
                                                }, {
                                                    color: "#FFBA00",
                                                    name: "Non-Critical",
                                                    data: [4]
                                                }, {
                                                    color: "#9299B0",
                                                    name: "Critical",
                                                    data: [0]
                                                }, {
                                                    color: "#d8ddf0",
                                                    name: "Unknown",
                                                    data: [21]
                                                }]
                                            }
                                        }
                                    }]
			                    }
			                }, {
			                    type: 'widgetGroup',
			                    options: {
			                        classes: 'blockBar blockBar-6',
			                        contents: [{
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Training" }, { className: 'name', data: "Element #1" }]
			                            }
			                        }, {
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Personnel" }, { className: 'name', data: "Element #2" }]
			                            }
			                        }, {
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Reliability" }, { className: 'name', data: "Element #3" }]
			                            }
			                        }, {
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Environment" }, { className: 'name', data: "Element #4" }]
			                            }
			                        }, {
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Integrity" }, { className: 'name', data: "Element #5" }]
			                            }
			                        }, {
			                            type: "basicWidget",
			                            options: {
			                                contents: [{ className: 'title', data: "Operations" }, { className: 'name', data: "Element #6" }]
			                            }
			                        }]
			                    }
			                }]
			            }
			        }, true ? {} : {
			            type: "basicPanel",
			            size: 12,
			            options: {
			                title: "Annual Analytics",
			                controlWidget: {
			                    type: "controlBar",
			                    options: {
			                        controls: [{
			                            type: 'controlSelect',
			                            options: {
			                                items: ["Atlantic Region",
                                                "Production and Operations",
                                                "Drillings and Completions"],
			                                width: 200,
			                                initialSelectedItemIndex: 0,
			                                classes: "r360-psDepartmentSelect"
			                            }
			                        }]
			                    }
			                },
			                maxHeight: mediumHeightOptions.maxHeight,
			                contents: [{
			                    type: "analyticsTrinityView",
			                    options: {
			                        colors: ["#74C4FD"],
			                        model: {
			                            labels: null,
			                            seriesKind: 'Type',
			                            series: [{
			                                name: "SeaRose TRIR Rolling",
			                                data: [[Date.UTC(2011, 9, 31), 0.54], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 0], [Date.UTC(2012, 0, 31), 0],
                                                    [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0.53], [Date.UTC(2012, 3, 30), 0.53], [Date.UTC(2012, 4, 31), 0.53],
                                                    [Date.UTC(2012, 5, 30), 0.51], [Date.UTC(2012, 6, 31), 0.47], [Date.UTC(2012, 7, 31), 0.93], [Date.UTC(2012, 8, 30), 1.4],
			                                        [Date.UTC(2012, 9, 31), 1.4], [Date.UTC(2012, 10, 30), 1.4], [Date.UTC(2012, 11, 31), 1.39], [Date.UTC(2013, 0, 31), 1.4],
                                                    [Date.UTC(2013, 1, 28), 1.4], [Date.UTC(2013, 2, 31), 1.4], [Date.UTC(2013, 3, 30), 1.41], [Date.UTC(2013, 4, 31), 1.41],
                                                    [Date.UTC(2013, 5, 30), 1.45], [Date.UTC(2013, 6, 31), 1.6], [Date.UTC(2013, 7, 31), 1.6], [Date.UTC(2013, 8, 30), 1.07]],
			                                marker: {
			                                    symbol: 'diamond',
			                                    fillColor: '#FFFFFF',
			                                    lineWidth: 2,
			                                    lineColor: null
			                                },
			                                zIndex: 2,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] }
			                            }], 
			                            xPlotLines: [{
			                                name: "Incident Report",
			                                color: 'red',
			                                marker: { enabled: false },
			                                intercepts: [Date.UTC(2011, 9, 28), Date.UTC(2011, 10, 2), Date.UTC(2011, 10, 7), Date.UTC(2012, 4, 30),
			                                    Date.UTC(2012, 6, 15), Date.UTC(2012, 7, 2), Date.UTC(2012, 9, 21), Date.UTC(2012, 10, 7)],
			                                details: ['IR-11-17114: Potential fall from height (HiPo NM)',
                                               'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                               'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)',
                                               'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                               'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System',
                                               'IR-12-15582: Fire in Aft Fire Pump Room',
                                               'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA',
                                               'IR-12-18011: Missing deck plate between decks']
			                            }]
			                        },
			                        chartView: {
			                            type: "lineChartView",
			                            options: this.analyticsTrinityViewChartViewOptions
			                        },
			                        legendView: {
			                            type: 'lineLegendView',
			                            options: { line: { chart: { height: 25 } } }
			                        },
			                        detailsView: {
			                            type: 'psInfoCardDetailsView'
			                        }
			                    }
			                }, {
			                    type: "analyticsTrinityView",
			                    options: {
			                        colors: ["#74C4FD", '#d8ddf0', '#0C8EEA', 'red'],
			                        model: {
			                            labels: null,
			                            seriesKind: 'Type',
			                            series: [{
			                                name: "Tier 1 PSE - SeaRose",
			                                data: [[Date.UTC(2011, 9, 31), 0], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 0], [Date.UTC(2012, 0, 31), 0],
                                                    [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0], [Date.UTC(2012, 3, 30), 0], [Date.UTC(2012, 4, 31), 0],
                                                    [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 0], [Date.UTC(2012, 8, 30), 1],
			                                        [Date.UTC(2012, 9, 31), 0], [Date.UTC(2012, 10, 30), 0], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 0],
                                                    [Date.UTC(2013, 1, 28), 0], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 0],
                                                    [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
			                                marker: {
			                                    symbol: 'diamond',
			                                    fillColor: '#FFFFFF',
			                                    lineWidth: 2,
			                                    lineColor: null
			                                },
			                                zIndex: 2,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                                kpi: { status: 'non-critical' }
			                            }, {
			                                name: "Tier 2 PSE - SeaRose",
			                                data: [[Date.UTC(2011, 9, 31), 0], [Date.UTC(2011, 10, 30), 1], [Date.UTC(2011, 11, 31), 3], [Date.UTC(2012, 0, 31), 0],
                                                    [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 0],
                                                    [Date.UTC(2012, 5, 30), 1], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 0], [Date.UTC(2012, 8, 30), 0],
			                                        [Date.UTC(2012, 9, 31), 0], [Date.UTC(2012, 10, 30), 1], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 1],
                                                    [Date.UTC(2013, 1, 28), 1], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 0],
                                                    [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
			                                marker: {
			                                    symbol: 'diamond',
			                                    fillColor: '#FFFFFF',
			                                    lineWidth: 2,
			                                    lineColor: null
			                                },
			                                zIndex: 2,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                                kpi: { status: 'non-critical' }
			                            }, {
			                                name: "Tier 3 PSE - SeaRose",
			                                data: [[Date.UTC(2011, 9, 31), 1], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 1], [Date.UTC(2012, 0, 31), 1],
                                                    [Date.UTC(2012, 1, 28), 1], [Date.UTC(2012, 2, 31), 1], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 0],
                                                    [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), 0], [Date.UTC(2012, 7, 31), 3], [Date.UTC(2012, 8, 30), 4],
			                                        [Date.UTC(2012, 9, 31), 3], [Date.UTC(2012, 10, 30), 2], [Date.UTC(2012, 11, 31), 0], [Date.UTC(2013, 0, 31), 3],
                                                    [Date.UTC(2013, 1, 28), 0], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 0], [Date.UTC(2013, 4, 31), 2],
                                                    [Date.UTC(2013, 5, 30), 1], [Date.UTC(2013, 6, 31), 0], [Date.UTC(2013, 7, 31), 0], [Date.UTC(2013, 8, 30), 0]],
			                                marker: {
			                                    symbol: 'square',
			                                    fillColor: '#FFFFFF',
			                                    lineWidth: 2,
			                                    lineColor: null
			                                },
			                                zIndex: 2,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] },
			                                kpi: { status: 'critical' }
			                            }], 
			                        xPlotLines: [{
			                            name: "Incident Report",
			                            color: 'red',
			                            marker: { enabled: false },
			                            intercepts: [Date.UTC(2011, 9, 28), Date.UTC(2011, 10, 2), Date.UTC(2011, 10, 7), Date.UTC(2012, 4, 30),
                                            Date.UTC(2012, 6, 15), Date.UTC(2012, 7, 2), Date.UTC(2012, 9, 21), Date.UTC(2012, 10, 7)],
			                            details: ['IR-11-17114: Potential fall from height (HiPo NM)',
                                           'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                           'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)',
                                           'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                           'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System',
                                           'IR-12-15582: Fire in Aft Fire Pump Room',
                                           'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA',
                                           'IR-12-18011: Missing deck plate between decks']
			                            }]
			                        },
			                        chartView: {
			                            type: "lineChartView",
			                            options: this.analyticsTrinityViewChartViewOptions
			                            //options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {
			                            //    xAxis: {
			                            //        plotLines: [{ // mark the weekend
			                            //            color: 'red',
			                            //            details: "IR-12-14862: Dropped Object from pipe barn crane (HiPo NM)",
			                            //            width: 2,
			                            //            value: Date.UTC(2014, 6, 1),
			                            //            events: {
			                            //                click: function (e) {

			                            //                },
			                            //                mouseover: function () {

			                            //                },
			                            //                mouseout: function () {

			                            //                }
			                            //            }
			                            //        }]
			                            //    },
			                            //})			                            
			                        },
			                        legendView: {
			                            type: 'kpiExtendedLineLegendView',
			                            options: { line: { chart: { height: 25 } } }
			                        },
			                        detailsView: {
			                            type: 'psInfoCardDetailsView'
			                        }
			                    }
			                }, {
			                    type: "analyticsTrinityView",
			                    options: {
			                        colors: ["#74C4FD", '#1BDD4E', 'red'],
			                        tickPositioner: { minOffset: 80 },
			                        model: {
			                            labels: null,
			                            seriesKind: 'Type',
			                            series: [{
			                                name: "% Training - SeaRose",
			                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                    [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                    [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
			                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                    [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                    [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],			                                
			                                marker: {    
			                                    symbol: 'diamond',
			                                        fillColor: '#FFFFFF',
			                                        lineWidth: 2,
			                                        lineColor: null
			                                },
			                                zIndex: 3,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] }
			                            }, {
			                                name: "Target",
			                                data: [[Date.UTC(2011, 9, 31), 90], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), null], [Date.UTC(2012, 0, 31), null],
                                                        [Date.UTC(2012, 1, 28), null], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), null], [Date.UTC(2012, 4, 31), null],
                                                        [Date.UTC(2012, 5, 30), null], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), null],
                                                        [Date.UTC(2012, 9, 31), null], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), null], [Date.UTC(2013, 0, 31), null],
                                                        [Date.UTC(2013, 1, 28), null], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), null], [Date.UTC(2013, 4, 31), null],
                                                        [Date.UTC(2013, 5, 30), null], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 90]],
			                                dashStyle: 'Dash',
			                                tooltip: { enabled: false },
			                                enableMouseTracking: false,
			                                allowPointSelect: false,
			                                zIndex: 2,
			                                marker: {
			                                    enabled: false,
			                                    states: { hover: { enabled: false }, select: { enabled: false } }
			                                }
			                            }],
			                            xPlotLines: [{
			                                name: "Incident Report",
			                                color: 'red',
                                            max: 100,
			                                marker: { enabled: false },
			                                intercepts: [Date.UTC(2011, 9, 28), Date.UTC(2011, 10, 2), Date.UTC(2011, 10, 7), Date.UTC(2012, 4, 30),
                                                Date.UTC(2012, 6, 15), Date.UTC(2012, 7, 2), Date.UTC(2012, 9, 21), Date.UTC(2012, 10, 7)],
			                                details: ['IR-11-17114: Potential fall from height (HiPo NM)',
                                               'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                               'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)',
                                               'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                               'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System',
                                               'IR-12-15582: Fire in Aft Fire Pump Room',
                                               'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA',
                                               'IR-12-18011: Missing deck plate between decks']
			                            }]
			                        },
			                        chartView: {
			                            type: "lineChartView",
			                            //options: this.analyticsTrinityViewChartViewOptions
			                            options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {
			                                yAxis: {
			                                    breaks: [{
			                                        from: 1,
			                                        to: 80,
			                                        breakSize: 0
			                                    }]
			                                }
			                            }),
			                        },
			                        legendView: {
			                            type: 'lineLegendView',
			                            options: { line: { chart: { height: 25 } } }
			                        },
			                        detailsView: {
			                            type: 'psInfoCardDetailsView'
			                        }
			                    }
			                }, {
			                    type: "analyticsTrinityView",
			                    options: {
			                        colors: ['#0C8EEA', 'red'],
			                        model: {
			                            labels: null,
			                            seriesKind: 'Type',
			                            series: [{
			                                name: "Monthly SR Trips",
			                                data: [[Date.UTC(2011, 9, 31), 7], [Date.UTC(2011, 10, 30), 2], [Date.UTC(2011, 11, 31), 5], [Date.UTC(2012, 0, 31), 5],
                                                    [Date.UTC(2012, 1, 28), 12], [Date.UTC(2012, 2, 31), 4], [Date.UTC(2012, 3, 30), 1], [Date.UTC(2012, 4, 31), 2],
                                                    [Date.UTC(2012, 5, 30), 0], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), null], [Date.UTC(2012, 8, 30), 12],
			                                        [Date.UTC(2012, 9, 31), 5], [Date.UTC(2012, 10, 30), 8], [Date.UTC(2012, 11, 31), 3], [Date.UTC(2013, 0, 31), 5],
                                                    [Date.UTC(2013, 1, 28), 3], [Date.UTC(2013, 2, 31), 0], [Date.UTC(2013, 3, 30), 4], [Date.UTC(2013, 4, 31), 1],
                                                    [Date.UTC(2013, 5, 30), 0], [Date.UTC(2013, 6, 31), 2], [Date.UTC(2013, 7, 31), 1], [Date.UTC(2013, 8, 30), 3]],
			                                marker: {
			                                    symbol: 'diamond',
			                                    fillColor: '#FFFFFF',
			                                    lineWidth: 2,
			                                    lineColor: null
			                                },
			                                zIndex: 2,
			                                details: { current: [1], ytd: [2], target: [3], type: [4] }
			                            }],
			                            xPlotLines: [{
			                                name: "Incident Report",
			                                color: 'red',
			                                marker: { enabled: false },
			                                intercepts: [Date.UTC(2011, 9, 28), Date.UTC(2011, 10, 2), Date.UTC(2011, 10, 7), Date.UTC(2012, 4, 30),
                                                Date.UTC(2012, 6, 15), Date.UTC(2012, 7, 2), Date.UTC(2012, 9, 21), Date.UTC(2012, 10, 7)],
			                                details: ['IR-11-17114: Potential fall from height (HiPo NM)',
                                               'IR-11-17154: Potential fall from height during survey (HiPo NM)',
                                               'IR-11-17279: Dropped Object from M03 PSV Deck (HiPo NM)',
                                               'IR-12-13877: Tow wire parted during transit <br> IR-12-IR-12-13105: Fire in Main Power Generator C',
                                               'IR-12-14996: Structural Connector Ball Valve failure on Hydraulic System',
                                               'IR-12-15582: Fire in Aft Fire Pump Room',
                                               'IR-12-17798: Change in plan during lift (HiPo NM) <br> IR-12-17803: System Stop Push Button remained active after GPA',
                                               'IR-12-18011: Missing deck plate between decks']
			                            }]
			                        },
			                        chartView: {
			                            type: "lineChartView",
			                            options: this.analyticsTrinityViewChartViewOptions
			                        },
			                        legendView: {
			                            type: 'lineLegendView',
			                            options: { line: { chart: { height: 25 } } }
			                        },
			                        detailsView: {
			                            type: 'psInfoCardDetailsView'
			                        }
			                    }
			                }]
			            }
			        }, {
			            type: "basicPanel",
			            size: 6,
			            options: {
			                maxHeight: mediumHeightOptions.maxHeight,
			                minHeight: mediumHeightOptions.maxHeight,
			                title: "Notable Trends",
			                controlWidget: !draft ? null : {
			                    type: "controlBar", options: {
			                        controls: [{
			                            type: "controlIconNavigate",
			                            options: {
			                                iconClassString: 'glyphicon glyphicon-plus-sign', panel: {
			                                    type: "editNotableTrendFormPanel",
			                                    options: {
			                                        title: "Add Notable Trend", maxHeight: mediumHeightOptions.maxHeight,
			                                        minHeight: mediumHeightOptions.maxHeight,
			                                        iconSelection: { icons: [{ iconClassString: "glyphicon glyphicon-ok" }, { iconClassString: "glyphicon glyphicon-arrow-up status-good" }, { iconClassString: "glyphicon glyphicon-arrow-down status-critical" }, { iconClassString: "glyphicon glyphicon-exclamation-sign status-non-critical" }] },
			                                        data: {
			                                            referenceList: ["Occupational Incident Rate (TRIR)",
                                                                        "Process Safety Events",
                                                                        "Spills & Unauthorized Discharges",
                                                                        "Near Miss Events",
                                                                        "Permit To Work & Isolation Events",
                                                                       "Timely Investigation of Serious+ Incidents",
                                                                        "HOIMSAR Documents",
                                                                        "Omnisafe Actions Items",
                                                                        "PHA Action Items",
                                                                        "Competency & Training",
                                                                        "Quarterly Shift Variances (Overtime)",
                                                                        "Inspections & SAS Audits",
                                                                        "Emergency Response Drills",
                                                                        "NOPRAS",
                                                                        "SeaRose Mechanical Integrity",
                                                                        "SeaRose Blocks, Alarms & Faults",
                                                                        "SeaRose Trips"]
			                                        }
			                                    }
			                                }
			                            }
			                        }]
			                    }
			                },
			                // INSERTHERE
			                contents: {
		                        type: "unnamed04",
		                        options: {
		                            data: [
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-arrow-up status-good",
                                                text: "There are 2 incidents remaining outstanding review for the last month.",
                                                elements: "#3, #4, #5",
                                                section: "Section 1A"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                classes: ".dash-separated",
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-ok",
                                                text: "Increase in alarms this past month due to faulty sensor.",
                                                elements: "#1, #4, #5",
                                                section: "Section 6C"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                classes: "dash-separated",
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        colors: ["#74C4FD", 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "Occupational Incidents",
                                                                data: [[Date.UTC(2011, 9, 31), 0.54], [Date.UTC(2011, 10, 30), 0], [Date.UTC(2011, 11, 31), 0], [Date.UTC(2012, 0, 31), 0],
                                                                        [Date.UTC(2012, 1, 28), 0], [Date.UTC(2012, 2, 31), 0.53], [Date.UTC(2012, 3, 30), 0.53], [Date.UTC(2012, 4, 31), 0.53],
                                                                        [Date.UTC(2012, 5, 30), 0.51], [Date.UTC(2012, 6, 31), 0.47], [Date.UTC(2012, 7, 31), 0.93], [Date.UTC(2012, 8, 30), 1.4],
                                                                        [Date.UTC(2012, 9, 31), 1.4], [Date.UTC(2012, 10, 30), 1.4], [Date.UTC(2012, 11, 31), 1.39], [Date.UTC(2013, 0, 31), 1.4],
                                                                        [Date.UTC(2013, 1, 28), 1.4], [Date.UTC(2013, 2, 31), 1.4], [Date.UTC(2013, 3, 30), 1.41], [Date.UTC(2013, 4, 31), 1.41],
                                                                        [Date.UTC(2013, 5, 30), 1.45], [Date.UTC(2013, 6, 31), 1.6], [Date.UTC(2013, 7, 31), 1.6], [Date.UTC(2013, 8, 30), 1.07]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 2,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }, {
                                                                name: "Incident Report",
                                                                type: 'column',
                                                                marker: { enabled: false },
                                                                allowPointSelect: false,
                                                                point: {
                                                                    events: {
                                                                        click: null,
                                                                        select: null,
                                                                        unselect: null
                                                                    }
                                                                },
                                                                lineWidth: 1,
                                                                zIndex: 1,
                                                                tooltip: {
                                                                    pointFormatter: function (e) {
                                                                        return this.series.userOptions.details[this.index];
                                                                    }
                                                                },
                                                                data: [[Date.UTC(2011, 9, 28), 2], [Date.UTC(2011, 10, 2), 2]],
                                                                details: ['4743-9457-8672 Ladder fall', '3473-4837-9047 Hydrocarbon Spill']
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    },
                                    {
                                        summary: {
                                            type: 'psInfoCardNotableTrend', options: {
                                                iconClass: "glyphicon glyphicon-exclamation-sign status-non-critical",
                                                text: "Near miss events have steadily increased in the past months. ",
                                                elements: "#1, #2, #3",
                                                section: "Near Miss Events"
                                            }
                                        },
                                        details: {
                                            type: 'widgetGroup',
                                            options: {
                                                contents: [{
                                                    type: "analyticsTrinityView",
                                                    options: {
                                                        tickPositioner: { minOffset: 80 },
                                                        colors: ["#74C4FD", '#1BDD4E', 'red'],
                                                        model: {
                                                            labels: null,
                                                            seriesKind: 'Type',
                                                            series: [{
                                                                name: "% Training",
                                                                data: [[Date.UTC(2011, 9, 31), 89], [Date.UTC(2011, 10, 30), null], [Date.UTC(2011, 11, 31), 92.55], [Date.UTC(2012, 0, 31), null],
                                                                        [Date.UTC(2012, 1, 28), 93.19], [Date.UTC(2012, 2, 31), null], [Date.UTC(2012, 3, 30), 92.55], [Date.UTC(2012, 4, 31), null],
                                                                        [Date.UTC(2012, 5, 30), 93.54], [Date.UTC(2012, 6, 31), null], [Date.UTC(2012, 7, 31), 93.5], [Date.UTC(2012, 8, 30), null],
                                                                        [Date.UTC(2012, 9, 31), 93.29], [Date.UTC(2012, 10, 30), null], [Date.UTC(2012, 11, 31), 93.16], [Date.UTC(2013, 0, 31), null],
                                                                        [Date.UTC(2013, 1, 28), 93.63], [Date.UTC(2013, 2, 31), null], [Date.UTC(2013, 3, 30), 93.33], [Date.UTC(2013, 4, 31), null],
                                                                        [Date.UTC(2013, 5, 30), 92.28], [Date.UTC(2013, 6, 31), null], [Date.UTC(2013, 7, 31), null], [Date.UTC(2013, 8, 30), 87.17]],
                                                                marker: {
                                                                    symbol: 'diamond',
                                                                    fillColor: '#FFFFFF',
                                                                    lineWidth: 2,
                                                                    lineColor: null
                                                                },
                                                                zIndex: 3,
                                                                details: { current: [1], ytd: [2], target: [3], type: [4] }
                                                            }]
                                                        },
                                                        chartView: {
                                                            type: "lineChartView",
                                                            options: this.analyticsTrinityViewChartViewOptions
                                                        },
                                                        legendView: {
                                                            type: 'kpiExtendedLineLegendView',
                                                            options: { line: { chart: { height: 25 } } }
                                                        },
                                                        detailsView: {
                                                            type: 'psInfoCardDetailsView'
                                                        }
                                                    }
                                                }]
                                            }
                                        }
                                    }]
		                        }
		                    }
			            }
			        }, {
			            type: "multiViewPanel",
			            size: 6,
			            options: {
			                title: 'Process Safety Events',
			                classes: "r360Panel-processSafetyEvents",
			                controlWidget: {
			                    type: 'controlBar',
			                    options: {
			                        controls: [{
			                            type: "buttonGroup",
			                            options: {
			                                buttons: [{ options: { text: "All", priorityElement: 'text' } },
                                                { options: { text: "Prod", priorityElement: 'text' } }, { options: { text: "Admin", priorityElement: 'text' } },
                                                { options: { text: "Finan", priorityElement: 'text' } }],
			                                buttonclick: onProcessSafetyButtonClick,
			                                initialPrimaryButtonIndex: 0
			                            }
			                        }, {
			                            type: 'tooltip',
			                            options: {
			                                contents: [['All', 'All Analytics'], ['Prod', 'Production'],
			                                    ['Admin', 'Administration'], ['Finan', 'Finance']],
			                                classes: 'fa fa-info-circle info-icon',
			                                customOptions: { placement: 'left' }
			                            }
			                        }]
			                    }
			                },
			                commonViewOptions: {
			                    colors: ["#1BDD4E", "#FFBA00", "#FF5A00", "#d8ddf0"],
			                    model: {
			                        labels: ["Good", "Non-Critical", 'Critical', 'Unknown'],
			                        seriesKind: 'KPI Status',
			                        dataKind: 'Count',
			                        series: [{
			                            name: "All",
			                            tag: "All Analytics",
			                            data: [9, 12, 9, 60],
			                            percents: [10, 13.3, 10, 66.7]
			                        }, {
			                            name: "Prod",
			                            tag: "Production",
			                            data: [1, 4, 2, 0],
			                            percents: [0.1, 14.3, 57.1, 28.6]
			                        }, {
			                            name: "Admin",
			                            tag: "Administration",
			                            data: [4, 0, 2, 5],
			                            percents: [36.4, 0, 18.2, 45.5]
			                        }, {
			                            name: "Finan",
			                            tag: "Finance",
			                            data: [0, 4, 0, 21],
			                            percents: [0, 16, 0, 84]
			                        }]
			                    }
			                },
			                views: [
                                {
                                    type: 'pieChartView',
                                    options: {
                                        showTagSubtitle: true,
                                        chart: { height: 350 }
                                    }
                                }, {
                                    type: 'singleSeriesLegendView'
                                }
			                ]
			            }
			        }, {
			            type: "tablePanel",
			            size: 6,
			            options: {
			                maxHeight: mediumHeightOptions.maxHeight,
			                title: "Notes for Report",
			                data: {
			                    cssClass: "table-hover",
			                    body: {
			                        rows: [{
			                            cells: [{
			                                type: "labelCell", options: {
			                                    title: "Current month missing all occupational incidents."
			                                }
			                            }, {
			                                type: "iconCell", options: { iconClassString: "glyphicon glyphicon-link" }
			                            }, (flags & 0x200) ? { type: "iconCell", options: { iconClassString: "glyphicon glyphicon-pencil" } } : null]
			                        }, {
			                            cells: [{
			                                type: "labelCell", options: {
			                                    title: "Targets for the year are currently under review. Expect target changes within the next month."
			                                }
			                            }, {
			                                type: "iconCell", options: { iconClassString: "glyphicon glyphicon-link" }
			                            }, (flags & 0x200) ? { type: "iconCell", options: { iconClassString: "glyphicon glyphicon-pencil" } } : null]
			                        }, {
			                            cells: [{
			                                type: "labelCell", options: {
			                                    title: "Overall values have declined since the previous report."
			                                }
			                            }, {
			                                type: "iconCell", options: { iconClassString: "glyphicon glyphicon-link" }
			                            }, (flags & 0x200) ? { type: "iconCell", options: { iconClassString: "glyphicon glyphicon-pencil" } } : null]
			                        }, {
			                            cells: [{
			                                type: "labelCell", options: {
			                                    title: "Data is complete this month, previous month was missing."
			                                }
			                            }, {
			                                type: "iconCell", options: { iconClassString: "glyphicon glyphicon-link" }
			                            }, (flags & 0x200) ? { type: "iconCell", options: { iconClassString: "glyphicon glyphicon-pencil" } } : null]
			                        }]
			                    }
			                },
			                controlWidget: {
			                    type: "controlBar", options: {
			                        controls: [!draft ? null : {
			                            type: "controlIconNavigate",
			                            options: {
			                                modal: { enabled: true, options: { showCloseBtn: false, enableEscapeKey: false } },
			                                iconClassString: 'glyphicon glyphicon-plus-sign', panel: {
			                                    type: "editKnownIssueFormPanel",
			                                    options: {
			                                        title: "Add Note for Report",
			                                        nextPanel: {
			                                            type: "editKnownIssueLinkPanel",
			                                            size: "12",
			                                            options: {
			                                                title: "Add Note for Report Link to Events",
			                                                renderHeader: true,
			                                                renderFooter: true,
			                                                contents: [{
			                                                    type: "labelCell",
			                                                    options: {
			                                                        title: "Note for report 1"
			                                                    }
			                                                }, {
			                                                    type: 'controlSelect',
			                                                    options: {
			                                                        items: [{
			                                                            groupName: 'Atlantic', groupItems: ["% Overdue Documents", "% Overdue Target", "% Criticality 1 Overdue", null,
                                                                            "All Documents", "Total Overdue", null, "Overdue Criticality 1", "Overdue Criticality 2", "Overdue Criticality 3",
                                                                            null, "% Overdue Actions", "% Overdue Target", "% High Priority Overdue", null, "Overdue Actions", null, "Open Actions", null, "Actions Closed During Month",
			                                                                null, "% Overdue PHA Actions", "..."]
			                                                        }, {
			                                                            groupName: 'Production Department', groupItems: ["Incident Rate",
                                                                           "Tier 1", "Tier 2", "Tier 3", null, "..."]
			                                                        }, {
			                                                            groupName: 'Finance Department', groupItems: ["Incident Rate", "..."]
			                                                        }, {
			                                                            groupName: 'Admin Department', groupItems: ["..."]
			                                                        }],
			                                                        multiple: true,
			                                                        label: 'Select Events: ',
			                                                        nonSelectedText: 'No Events Selected',
			                                                        nSelectedText: " Events Selected",
			                                                        enableFiltering: true,
			                                                        enableCaseInsensitiveFiltering: true,
			                                                        includeSelectAllOption: false,
			                                                        width: 280,
			                                                        height: 450,
			                                                        open: refreshEditKnownIssueLinkPanel,
			                                                        close: refreshEditKnownIssueLinkPanel,
			                                                        change: onEditKnownIssueSelectChange
			                                                    }
			                                                }, {
			                                                    type: 'widgetGroup',
			                                                    options: {
			                                                        contents: []
			                                                    }
			                                                }]
			                                            }
			                                        }
			                                    }
			                                }
			                            }
			                        }]
			                    }
			                }
			            }
			        }, {
						type: "basicPanel",
						size: 6,
						options: {
							maxHeight: mediumHeightOptions.maxHeight,
							minHeight: mediumHeightOptions.maxHeight,
							title: "Leading and Lagging Indicators",
							contents: [{ type: "iconCell", options: { iconClassString: "psHomeLeadingLaggingTODO glyphicon glyphicon-question-sign" } }]
						}
					}, {
						type: "groupPanel",
						size: "12",
						options: {
							title: "Report Summary",
							tabbed: true,
							controlWidget: {
								type: "controlBar",
								options: {
								    controls: tabControls
								}
							},
							contents: [{
								type: "basicPanel",
								size: 12,
								options: {
									title: "Process Safety Events",
									classes: "r360-psReportSummaryPanel",
									controlWidget: {
										type: 'controlBar',
										options: {
											controls: [{
												type: 'controlSelect',
												options: {
												    items: ["Occupational Incident Rate",
                                                            "Documents"],
													width: 290,
													initialSelectedItemIndex: 6,
													classes: "r360-psSectionSelect",
													close: onReportSummarySelectionChange
												}
											}]
										}
									},
									contents: that.reportSummaryWidgets['Documents']
								}
							}
							]
						}
					}, {
						type: "groupPanel",
						size: "12",
						options: {
							title: "Events",
							tabbed: true,
							controlWidget: {
								type: "controlBar",
								options: {
								    controls: tabControls
								}
							},
							contents: [{
							    type: "basicPanel",
							    size: 12,
							    options: {
							        renderHeader: false,
							        renderFooter: false,
							        classes: "r360-psEventsHeroBoxGroupPanel",
							        contents: [{
							            type: "heroBoxGroupView",
							            options: {
							                model: {
							                    heroBoxes: [{ type: 'basicWidgetTabular', options: { contents: [{ className: 'title', data: "All Analytics" }] }},
							                    { type: 'basicWidgetTabular', options: { contents: [{ className: 'title', data: "Production" }] } },
							                    { type: 'basicWidgetTabular', options: { contents: [{ className: 'title', data: "Finance" }] } }]
							                }
							            }
							        }]
							    }
							}, {
								type: "basicPanel",
								size: 12,
								options: {
									title: "Leading and Lagging Metrics",
									classes: "r360-psEventsLeadingLaggingPanel",
									controlWidget: {
										type: 'controlBar', options: {
											controls: [{
												type: 'controlSelectHoimsElements',
												options: {
													items: ["Element #1 - Training", "Element #2 - Personnel",
                                                        "Element #3 - Reliability", "Element #4 - Environment",
                                                        "Element #5 - Integrity", "Element #6 - Operations"],
													multiple: true,
													width: 388,
													classes: "r360-psEventsLeadingLaggingSelect"
												}
											}]
										}
									},
									maxHeight: largeHeightOptions.maxHeight,
									contents: [
                                        {
                                        	type: 'widgetGroup',
                                        	options: {
                                        		idClass: 'lagging',
                                        		contents: [{
                                        			type: "analyticsTrinityView",
                                        			options: {
                                        				colors: ["#74C4FD", '#1BDD4E', '#0C8EEA'],
                                        				model: {
                                        					labels: null,
                                        					seriesKind: 'Type',
                                        					series: [{
                                        						name: "% Overdue Documents",
                                        						data: [[Date.UTC(2013, 9, 31), 5.31], [Date.UTC(2013, 10, 30), 6.78], [Date.UTC(2013, 11, 31), 6.14], [Date.UTC(2014, 0, 31), 8.10],
                                                                   [Date.UTC(2014, 1, 28), 8.21], [Date.UTC(2014, 2, 31), 6.13], [Date.UTC(2014, 3, 30), 7.22], [Date.UTC(2014, 4, 31), 7.72],
                                                                   [Date.UTC(2014, 5, 30), 7.83], [Date.UTC(2014, 6, 31), 7.94], [Date.UTC(2014, 7, 31), 10.21], [Date.UTC(2014, 8, 30), 6.24]],
                                        						marker: {
                                        							symbol: 'square',
                                        							fillColor: '#FFFFFF',
                                        							lineWidth: 2,
                                        							lineColor: null
                                        						},
                                        						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                        						kpi: { status: 'critical' }
                                        					}, {
                                        						name: "% Overdue Target",
                                        						data: [[Date.UTC(2013, 9, 31), 5], [Date.UTC(2013, 10, 30), null], [Date.UTC(2013, 11, 31), null], [Date.UTC(2014, 0, 31), null],
                                                                    [Date.UTC(2014, 1, 28), null], [Date.UTC(2014, 2, 31), null], [Date.UTC(2014, 3, 30), null], [Date.UTC(2014, 4, 31), null],
                                                                    [Date.UTC(2014, 5, 30), null], [Date.UTC(2014, 6, 31), null], [Date.UTC(2014, 7, 31), null], [Date.UTC(2014, 8, 30), 5]],
                                        						dashStyle: 'Dash',
                                        						tooltip: { enabled: false },
                                        						enableMouseTracking: false,
                                        						allowPointSelect: false,
                                        						marker: {
                                        							enabled: false,
                                        							states: { hover: { enabled: false }, select: { enabled: false } }
                                        						},
                                        					    kpi: { status: null }
                                        					}, {
                                        						name: "% Criticality 1 Overdue",
                                        						data: [[Date.UTC(2013, 9, 31), 1.11], [Date.UTC(2013, 10, 30), 1.48], [Date.UTC(2013, 11, 31), 0.98], [Date.UTC(2014, 0, 31), 1.60],
                                                                    [Date.UTC(2014, 1, 28), 1.35], [Date.UTC(2014, 2, 31), 0.74], [Date.UTC(2014, 3, 30), 0.98], [Date.UTC(2014, 4, 31), 1.10],
                                                                    [Date.UTC(2014, 5, 30), 1.22], [Date.UTC(2014, 6, 31), 1.10], [Date.UTC(2014, 7, 31), 0.98], [Date.UTC(2014, 8, 30), 0.50]],
                                        						marker: {
                                        							symbol: 'diamond',
                                        							fillColor: '#FFFFFF',
                                        							lineWidth: 2,
                                        							lineColor: null
                                        						},
                                        						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                        						kpi: { status: 'non-critical' }
                                        					}]
                                        				},
                                        				chartView: {
                                        					type: "lineChartView",
                                        					options: this.analyticsTrinityViewChartViewOptions
                                        				},
                                        				legendView: {
                                        					type: 'kpiExtendedLineLegendView',
                                        					options: { line: { chart: { height: 25 } } }
                                        				},
                                        				detailsView: {
                                        					type: 'psInfoCardDetailsView'
                                        				}
                                        			}
                                        		}, {
                                        			type: "analyticsTrinityView",
                                        			options: {
                                        				colors: ['#0C8EEA', "#74C4FD", "#2f89cd"],
                                        				model: {
                                        					labels: null,
                                        					seriesKind: 'Type',
                                        					series: [{
                                        						name: "All Documents",
                                        						data: [[Date.UTC(2013, 9, 31), 810], [Date.UTC(2013, 10, 30), 811], [Date.UTC(2013, 11, 31), 814], [Date.UTC(2014, 0, 31), 815],
                                                                    [Date.UTC(2014, 1, 28), 816], [Date.UTC(2014, 2, 31), 816], [Date.UTC(2014, 3, 30), 817], [Date.UTC(2014, 4, 31), 816],
                                                                    [Date.UTC(2014, 5, 30), 817], [Date.UTC(2014, 6, 31), 819], [Date.UTC(2014, 7, 31), 813], [Date.UTC(2014, 8, 30), 801]],
                                        						marker: {
                                        							symbol: 'diamond',
                                        							fillColor: '#FFFFFF',
                                        							lineWidth: 2,
                                        							lineColor: null
                                        						},
                                        						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                        						kpi: null
                                        					}, {
                                        						name: "Total Overdue",
                                        						data: [[Date.UTC(2013, 9, 31), 43], [Date.UTC(2013, 10, 30), 50], [Date.UTC(2013, 11, 31), 55], [Date.UTC(2014, 0, 31), 66],
                                                                    [Date.UTC(2014, 1, 28), 67], [Date.UTC(2014, 2, 31), 50], [Date.UTC(2014, 3, 30), 59], [Date.UTC(2014, 4, 31), 63],
                                                                    [Date.UTC(2014, 5, 30), 64], [Date.UTC(2014, 6, 31), 65], [Date.UTC(2014, 7, 31), 83], [Date.UTC(2014, 8, 30), 50]],
                                        						marker: {
                                        							symbol: 'square',
                                        							fillColor: '#FFFFFF',
                                        							lineWidth: 2,
                                        							lineColor: null
                                        						},
                                        						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                        						kpi: null
                                        					}]
                                        				},
                                        				chartView: {
                                        					type: "lineChartView",
                                        					options: this.analyticsTrinityViewChartViewOptions
                                        				},
                                        				legendView: {
                                        					type: 'kpiExtendedLineLegendView',
                                        					options: { line: { chart: { height: 25 } } }
                                        				},
                                        				detailsView: {
                                        					type: 'psInfoCardDetailsView'
                                        				}
                                        			}
                                        		}]
                                        	}
                                        }
                                    , {
                                    	type: 'widgetGroup',
                                    	options: {
                                    		idClass: 'leading',
                                    		contents: [{
                                    			type: "analyticsTrinityView",
                                    			options: {
                                    				colors: ['#0C8EEA', "#74C4FD", "#d8ddf0"],
                                    				model: {
                                    					labels: null,
                                    					seriesKind: 'Type',
                                    					series: [{
                                    						name: "Overdue Criticality 1",
                                    						data: [[Date.UTC(2013, 9, 31), 9], [Date.UTC(2013, 10, 30), 12], [Date.UTC(2013, 11, 31), 8], [Date.UTC(2014, 0, 31), 13],
                                                                [Date.UTC(2014, 1, 28), 11], [Date.UTC(2014, 2, 31), 6], [Date.UTC(2014, 3, 30), 8], [Date.UTC(2014, 4, 31), 9],
                                                                [Date.UTC(2014, 5, 30), 10], [Date.UTC(2014, 6, 31), 9], [Date.UTC(2014, 7, 31), 8], [Date.UTC(2014, 8, 30), 4]],
                                    						marker: {
                                    							symbol: 'diamond',
                                    							fillColor: '#FFFFFF',
                                    							lineWidth: 2,
                                    							lineColor: null
                                    						},
                                    						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                    						kpi: null
                                    					}, {
                                    						name: "Overdue Criticality 2",
                                    						data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 22], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 29],
                                                                [Date.UTC(2014, 1, 28), 31], [Date.UTC(2014, 2, 31), 24], [Date.UTC(2014, 3, 30), 26], [Date.UTC(2014, 4, 31), 29],
                                                                [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 31], [Date.UTC(2014, 7, 31), 46], [Date.UTC(2014, 8, 30), 25]],
                                    						marker: {
                                    							symbol: 'square',
                                    							fillColor: '#FFFFFF',
                                    							lineWidth: 2,
                                    							lineColor: null
                                    						},
                                    						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                    						kpi: null
                                    					}, {
                                    						name: "Overdue Criticality 3",
                                    						data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 21], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 24],
                                                                [Date.UTC(2014, 1, 28), 25], [Date.UTC(2014, 2, 31), 20], [Date.UTC(2014, 3, 30), 25], [Date.UTC(2014, 4, 31), 25],
                                                                [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 25], [Date.UTC(2014, 7, 31), 29], [Date.UTC(2014, 8, 30), 21]],
                                    						marker: {
                                    							symbol: 'triangle',
                                    							fillColor: '#FFFFFF',
                                    							lineWidth: 2,
                                    							lineColor: null
                                    						},
                                    						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                    						kpi: null
                                    					}]
                                    				},
                                    				chartView: {
                                    					type: "lineChartView",
                                    					options: this.analyticsTrinityViewChartViewOptions
                                    				},
                                    				legendView: {
                                    					type: 'kpiExtendedLineLegendView',
                                    					options: { line: { chart: { height: 25 } } }
                                    				},
                                    				detailsView: {
                                    					type: 'psInfoCardDetailsView'
                                    				}
                                    			}
                                    		}, {
                                    			type: "analyticsTrinityView",
                                    			options: {
                                    				colors: ["#74C4FD", '#1BDD4E', '#0C8EEA'],
                                    				model: {
                                    					labels: null,
                                    					seriesKind: 'Type',
                                    					series: [{
                                    						name: "% Overdue Documents",
                                    						data: [[Date.UTC(2013, 9, 31), 5.31], [Date.UTC(2013, 10, 30), 6.78], [Date.UTC(2013, 11, 31), 6.14], [Date.UTC(2014, 0, 31), 8.10],
                                                               [Date.UTC(2014, 1, 28), 8.21], [Date.UTC(2014, 2, 31), 6.13], [Date.UTC(2014, 3, 30), 7.22], [Date.UTC(2014, 4, 31), 7.72],
                                                               [Date.UTC(2014, 5, 30), 7.83], [Date.UTC(2014, 6, 31), 7.94], [Date.UTC(2014, 7, 31), 10.21], [Date.UTC(2014, 8, 30), 6.24]],
                                    						marker: {
                                    							symbol: 'square',
                                    							fillColor: '#FFFFFF',
                                    							lineWidth: 2,
                                    							lineColor: null
                                    						},
                                    						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                    						kpi: { status: 'critical' }
                                    					}, {
                                    						name: "% Overdue Target",
                                    						data: [[Date.UTC(2013, 9, 31), 5], [Date.UTC(2013, 10, 30), null], [Date.UTC(2013, 11, 31), null], [Date.UTC(2014, 0, 31), null],
                                                                [Date.UTC(2014, 1, 28), null], [Date.UTC(2014, 2, 31), null], [Date.UTC(2014, 3, 30), null], [Date.UTC(2014, 4, 31), null],
                                                                [Date.UTC(2014, 5, 30), null], [Date.UTC(2014, 6, 31), null], [Date.UTC(2014, 7, 31), null], [Date.UTC(2014, 8, 30), 5]],
                                    						dashStyle: 'Dash',
                                    						tooltip: { enabled: false },
                                    						enableMouseTracking: false,
                                    						allowPointSelect: false,
                                    						marker: {
                                    							enabled: false,
                                    							states: { hover: { enabled: false }, select: { enabled: false } }
                                    						},
                                    					    kpi: { status: null }
                                    					}, {
                                    						name: "% Criticality 1 Overdue",
                                    						data: [[Date.UTC(2013, 9, 31), 1.11], [Date.UTC(2013, 10, 30), 1.48], [Date.UTC(2013, 11, 31), 0.98], [Date.UTC(2014, 0, 31), 1.60],
                                                                [Date.UTC(2014, 1, 28), 1.35], [Date.UTC(2014, 2, 31), 0.74], [Date.UTC(2014, 3, 30), 0.98], [Date.UTC(2014, 4, 31), 1.10],
                                                                [Date.UTC(2014, 5, 30), 1.22], [Date.UTC(2014, 6, 31), 1.10], [Date.UTC(2014, 7, 31), 0.98], [Date.UTC(2014, 8, 30), 0.50]],
                                    						marker: {
                                    							symbol: 'diamond',
                                    							fillColor: '#FFFFFF',
                                    							lineWidth: 2,
                                    							lineColor: null
                                    						},
                                    						details: { current: [1], ytd: [2], target: [3], type: [4] },
                                    						kpi: { status: 'non-critical' }
                                    					}]
                                    				},
                                    				chartView: {
                                    					type: "lineChartView",
                                    					options: this.analyticsTrinityViewChartViewOptions
                                    				},
                                    				legendView: {
                                    					type: 'kpiExtendedLineLegendView',
                                    					options: { line: { chart: { height: 25 } } }
                                    				},
                                    				detailsView: {
                                    					type: 'psInfoCardDetailsView'
                                    				}
                                    			}
                                    		}]
                                    	}
                                    }]
								}
							}
							]
						}
					}, {
						type: "groupPanel",
						size: "12",
						options: {
							title: "Overdue",
							tabbed: true,
							controlWidget: {
								type: "controlBar",
								options: {
									controls: overdueTabControls
								}
							},
							contents: [{
								type: "basicPanel",
								size: 12,
								options: {
									renderHeader: false,
									renderFooter: false,
									contents: [{
										type: "heroBoxGroupView",
										options: {
											model: {
												heroBoxes: [{ primary: "HOIMS", secondary: "", tertiary: "", description: "" }, { primary: "Omnisafe", secondary: "", tertiary: "", description: "" }, { primary: "PHA", secondary: "", tertiary: "", description: "" }]
											}
										}
									}]
								}
							}, {
								type: "basicPanel",
								size: 4,
								options: {
								    title: "% of Overdue",
								    minHeight: 300,
									contents: [{
										type: "analyticsTrinityView",
										options: {
											colors: ["#74C4FD", '#1BDD4E', '#0C8EEA'],
											model: {
												//labels: null,
												seriesKind: 'Type',
												series: [{
													name: "% Overdue Docs",
													data: [[Date.UTC(2013, 9, 31), 5.31], [Date.UTC(2013, 10, 30), 6.78], [Date.UTC(2013, 11, 31), 6.14], [Date.UTC(2014, 0, 31), 8.10],
                                                       [Date.UTC(2014, 1, 28), 8.21], [Date.UTC(2014, 2, 31), 6.13], [Date.UTC(2014, 3, 30), 7.22], [Date.UTC(2014, 4, 31), 7.72],
                                                       [Date.UTC(2014, 5, 30), 7.83], [Date.UTC(2014, 6, 31), 7.94], [Date.UTC(2014, 7, 31), 10.21], [Date.UTC(2014, 8, 30), 6.24]],
													marker: {
														symbol: 'square',
														fillColor: '#FFFFFF',
														lineWidth: 2,
														lineColor: null
													},
													details: { current: [1], ytd: [2], target: [3], type: [4] },
													kpi: { status: 'critical' }
												}, {
													name: "% Overdue Target",
													data: [[Date.UTC(2013, 9, 31), 5], [Date.UTC(2013, 10, 30), null], [Date.UTC(2013, 11, 31), null], [Date.UTC(2014, 0, 31), null],
                                                        [Date.UTC(2014, 1, 28), null], [Date.UTC(2014, 2, 31), null], [Date.UTC(2014, 3, 30), null], [Date.UTC(2014, 4, 31), null],
                                                        [Date.UTC(2014, 5, 30), null], [Date.UTC(2014, 6, 31), null], [Date.UTC(2014, 7, 31), null], [Date.UTC(2014, 8, 30), 5]],
													dashStyle: 'Dash',
													tooltip: { enabled: false },
													enableMouseTracking: false,
													allowPointSelect: false,
													marker: {
														enabled: false,
														states: { hover: { enabled: false }, select: { enabled: false } }
													},
													kpi: { status: null }
												}, {
													name: "% Criticality 1 Overdue",
													data: [[Date.UTC(2013, 9, 31), 1.11], [Date.UTC(2013, 10, 30), 1.48], [Date.UTC(2013, 11, 31), 0.98], [Date.UTC(2014, 0, 31), 1.60],
                                                        [Date.UTC(2014, 1, 28), 1.35], [Date.UTC(2014, 2, 31), 0.74], [Date.UTC(2014, 3, 30), 0.98], [Date.UTC(2014, 4, 31), 1.10],
                                                        [Date.UTC(2014, 5, 30), 1.22], [Date.UTC(2014, 6, 31), 1.10], [Date.UTC(2014, 7, 31), 0.98], [Date.UTC(2014, 8, 30), 0.50]],
													marker: {
														symbol: 'diamond',
														fillColor: '#FFFFFF',
														lineWidth: 2,
														lineColor: null
													},
													details: { current: [1], ytd: [2], target: [3], type: [4] },
													kpi: { status: 'non-critical' }
												}]
											},
											chartView: {
												type: "lineChartView",
												options: this.analyticsTrinityViewChartViewOptions
											},
											legendView: {
												type: 'kpiExtendedLineLegendView',
												options: { line: { chart: { height: 20 } } }
											},
											detailsView: {
												type: 'psInfoCardDetailsView'
											}
										}
									}]
								}
							}
                            , {
                            	type: "basicPanel",
                            	size: 4,
                            	options: {
                            		title: "Total Overdue",
                                    minHeight: 300,
                            		contents: [{
                            		    type: "analyticsTrinityView",
                            			options: {
                            				colors: ['#0C8EEA', "#74C4FD", "#2f89cd"],
                            				model: {
                            					labels: null,
                            					seriesKind: 'Type',
                            					series: [{
                            						name: "All Documents",
                            						data: [[Date.UTC(2013, 9, 31), 810], [Date.UTC(2013, 10, 30), 811], [Date.UTC(2013, 11, 31), 814], [Date.UTC(2014, 0, 31), 815],
                                                        [Date.UTC(2014, 1, 28), 816], [Date.UTC(2014, 2, 31), 816], [Date.UTC(2014, 3, 30), 817], [Date.UTC(2014, 4, 31), 816],
                                                        [Date.UTC(2014, 5, 30), 817], [Date.UTC(2014, 6, 31), 819], [Date.UTC(2014, 7, 31), 813], [Date.UTC(2014, 8, 30), 801]],
                            						marker: {
                            							symbol: 'diamond',
                            							fillColor: '#FFFFFF',
                            							lineWidth: 2,
                            							lineColor: null
                            						},
                            						details: { current: [1], ytd: [2], target: [3], type: [4] },
                            						kpi: null
                            					}, {
                            						name: "Total Overdue",
                            						data: [[Date.UTC(2013, 9, 31), 43], [Date.UTC(2013, 10, 30), 50], [Date.UTC(2013, 11, 31), 55], [Date.UTC(2014, 0, 31), 66],
                                                        [Date.UTC(2014, 1, 28), 67], [Date.UTC(2014, 2, 31), 50], [Date.UTC(2014, 3, 30), 59], [Date.UTC(2014, 4, 31), 63],
                                                        [Date.UTC(2014, 5, 30), 64], [Date.UTC(2014, 6, 31), 65], [Date.UTC(2014, 7, 31), 83], [Date.UTC(2014, 8, 30), 50]],
                            						marker: {
                            							symbol: 'square',
                            							fillColor: '#FFFFFF',
                            							lineWidth: 2,
                            							lineColor: null
                            						},
                            						details: { current: [1], ytd: [2], target: [3], type: [4] },
                            						kpi: null
                            					}]
                            				},
                            				chartView: {
                            					type: "lineChartView",
                            					options: this.analyticsTrinityViewChartViewOptions
                            				},
                            				legendView: {
                            					type: 'kpiExtendedLineLegendView',
                            					options: { line: { chart: { height: 25 } } }
                            				},
                            				detailsView: {
                            					type: 'psInfoCardDetailsView'
                            				}
                            			}
                            		}]
                            	}
                            }, {
                            	type: "basicPanel",
                            	size: 4,
                            	options: {
                            	    title: "Criticality",
                            	    minHeight: 300,
                            		contents: [{
                            			type: "analyticsTrinityView",
                            			options: {
                            				colors: ['#0C8EEA', "#74C4FD", "#d8ddf0"],
                            				model: {
                            					labels: null,
                            					seriesKind: 'Type',
                            					series: [{
                            						name: "Overdue Criticality 1",
                            						data: [[Date.UTC(2013, 9, 31), 9], [Date.UTC(2013, 10, 30), 12], [Date.UTC(2013, 11, 31), 8], [Date.UTC(2014, 0, 31), 13],
                                                        [Date.UTC(2014, 1, 28), 11], [Date.UTC(2014, 2, 31), 6], [Date.UTC(2014, 3, 30), 8], [Date.UTC(2014, 4, 31), 9],
                                                        [Date.UTC(2014, 5, 30), 10], [Date.UTC(2014, 6, 31), 9], [Date.UTC(2014, 7, 31), 8], [Date.UTC(2014, 8, 30), 4]],
                            						marker: {
                            							symbol: 'diamond',
                            							fillColor: '#FFFFFF',
                            							lineWidth: 2,
                            							lineColor: null
                            						},
                            						details: { current: [1], ytd: [2], target: [3], type: [4] },
                            						kpi: null
                            					}, {
                            						name: "Overdue Criticality 2",
                            						data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 22], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 29],
                                                        [Date.UTC(2014, 1, 28), 31], [Date.UTC(2014, 2, 31), 24], [Date.UTC(2014, 3, 30), 26], [Date.UTC(2014, 4, 31), 29],
                                                        [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 31], [Date.UTC(2014, 7, 31), 46], [Date.UTC(2014, 8, 30), 25]],
                            						marker: {
                            							symbol: 'square',
                            							fillColor: '#FFFFFF',
                            							lineWidth: 2,
                            							lineColor: null
                            						},
                            						details: { current: [1], ytd: [2], target: [3], type: [4] },
                            						kpi: null
                            					}, {
                            						name: "Overdue Criticality 3",
                            						data: [[Date.UTC(2013, 9, 31), 17], [Date.UTC(2013, 10, 30), 21], [Date.UTC(2013, 11, 31), 21], [Date.UTC(2014, 0, 31), 24],
                                                        [Date.UTC(2014, 1, 28), 25], [Date.UTC(2014, 2, 31), 20], [Date.UTC(2014, 3, 30), 25], [Date.UTC(2014, 4, 31), 25],
                                                        [Date.UTC(2014, 5, 30), 27], [Date.UTC(2014, 6, 31), 25], [Date.UTC(2014, 7, 31), 29], [Date.UTC(2014, 8, 30), 21]],
                            						marker: {
                            							symbol: 'triangle',
                            							fillColor: '#FFFFFF',
                            							lineWidth: 2,
                            							lineColor: null
                            						},
                            						details: { current: [1], ytd: [2], target: [3], type: [4] },
                            						kpi: null
                            					}]
                            				},
                            				chartView: {
                            					type: "lineChartView",
                            					options: $.extend(true, {}, this.analyticsTrinityViewChartViewOptions, {
                            						//selectPointOnHover: true,
                            						yAxis: {
                            							tickPositioner: function () {
                            								var positions = [];
                            								var scaledRange = this.dataMax - this.dataMin;
                            								var digitsBelowZero = 0;
                            								while (scaledRange < 1) {
                            									digitsBelowZero += 1;
                            									scaledRange *= 10;
                            								}
                            								var scaler = Math.pow(10, digitsBelowZero + 1);
                            								var bottomTick = Math.floor((this.dataMin * scaler) / scaler);
                            								var topTick = Math.floor(this.dataMax * scaler) / scaler;
                            								var centerTick = Math.floor((((topTick - bottomTick) / 2) + bottomTick) * scaler) / scaler;
                            								var centerBottomTick = Math.floor((((centerTick - bottomTick) / 2) + bottomTick) * scaler) / scaler;
                            								var centerTopTick = Math.floor((((topTick - centerTick) / 2) + centerTick) * scaler) / scaler;
                            								positions.push(bottomTick);
                            								//positions.push(centerBottomTick);
                            								positions.push(centerTick);
                            								//positions.push(centerTopTick);
                            								positions.push(topTick);
                            								return positions;
                            							}
                            						},
                            						chart: { height: 150 },
                            						plotOptions: { line: { connectNulls: true } }
                            					})
                            				},
                            				legendView: {
                            					type: 'kpiExtendedLineLegendView',
                            					options: { line: { chart: { height: 25 } } }
                            				},
                            				detailsView: {
                            					type: 'psInfoCardDetailsView'
                            				}
                            			}
                            		}]
                            	}
                            }]
						}
					}]
				}
			};
		}
	}
	return exports;
})(jQuery);
