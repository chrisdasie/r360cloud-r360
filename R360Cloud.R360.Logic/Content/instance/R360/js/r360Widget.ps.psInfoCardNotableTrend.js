/**

@class r360View.psInfoCardDetailsView
@extends radient360Widget.abstractWidget
*/
$.widget("r360Widget.psInfoCardNotableTrend", $.radient360Widget.abstractWidget, /**@lends r360Widget.psInfoCardNotableTrend*/ {
    options: {
        iconClass: "glyphicon glyphicon-arrow-up status-good",
        text: "There were 2 major incidents in latest month which are still under investigation.",
        elements: "#3, #5",
        section: "Major Incidents"
    },

    _create: function () {
        var that = this;
        this._super();

        var hash = $(location).attr('hash');
        var flags = parseInt(hash.slice(9, hash.length), 16);
        this.draftMode = flags & 0x200;

        var contentString = "";
        contentString += "<div class='" + this.contextClass + "-wrapper'>";
        contentString += "<div class='" + this.contextClass + "-icon'></div>";
        contentString += this.draftMode ? "<div class='" + this.contextClass + "-control glyphicon glyphicon-pencil'></div>" : "";
        contentString += "<div class='" + this.contextClass + "-text'></div>";
        contentString += "<div class='" + this.contextClass + "-section'><div class='" + this.contextClass + "-data'></div><div class='" + this.contextClass + "-title'></div></div>";
        contentString += "<div class='" + this.contextClass + "-elements'><div class='" + this.contextClass + "-data'></div><div class='" + this.contextClass + "-title'></div></div>";
        contentString += "</div>";
        this.element.append(contentString);

        this.element.find("." + this.contextClass + "-icon").addClass(this.options.iconClass);

        this.element.find("." + this.contextClass + "-text").text(this.options.text);

        this.element.find("." + this.contextClass + "-section ." + this.contextClass + "-title").text("Document Section");
        this.element.find("." + this.contextClass + "-section ." + this.contextClass + "-data").text(this.options.section);

        this.element.find("." + this.contextClass + "-elements ." + this.contextClass + "-title").text("HOIMS Elements");
        this.element.find("." + this.contextClass + "-elements ." + this.contextClass + "-data").text(this.options.elements);
    },

    _destroy: function () {
        this.element.empty();
        this._super();
    }
});