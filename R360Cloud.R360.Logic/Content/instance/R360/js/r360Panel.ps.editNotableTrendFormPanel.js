/** 
This class has the logic required to implement the PS add notable trend workflow formset.

@class r360Panel.editNotableTrendFormPanel 
@extends radient360Panel.formPanel
*/
$.widget("r360Panel.editNotableTrendFormPanel", $.radient360Panel.formPanel, /** @lends r360Panel.editNotableTrendFormPanel */ {
    options: {
        addSave: true,
        data: { referenceList: null, text: null },
        iconSelection: null
    },

    _create: function () {
        this._super();
        this.element.find("." + this.iconSelectionClass).controlIconSelector({ iconSelection: this.options.iconSelection });
        this.element.find("." + this.referenceSelectionClass).controlSelect({
            items: this.options.data.referenceList,
            nonSelectedText: 'Select Linked Section',
            width: 290,
            height: 240,
            dropup: true,
            classes: "r360-psSectionSelect"
        });
    },

    getPanelBodyContentString: function () {
        var that = this;
        this.iconSelectionClass = 'icon-selection';
        this.referenceSelectionClass = 'reference-selection';
        var body = this._super();
        body += "<form><table class='" + this.contextClass + "-table'><tbody>";
        body += "<tr><td><label>Notable Trend:</label></td><td><textarea rows=8>" + (this.options.data.text ? this.options.data.text : "") + "</textarea></td></tr>";
        body += "<tr><td><label>Icon:</label></td><td><div class='" + this.iconSelectionClass + "'></div></td></tr>";
        if (this.options.data.referenceList) {
            body += "<tr><td><label>Reference:</label></td><td class='" + this.referenceSelectionClass + "'></td></tr>";
        }
        body += "</tbody></table></form>";
        return body;
    },

    _cancel: function () {
        // TODO handle modal better
        var $modal = this.element.closest(".radient360Widget-modal");
        if ($modal.length > 0) {
            $modal.modal("close");
        }
        else {
            this._super();
        }
    },

    _save: function () {
        this._cancel();
    },

    _destroy: function () {
        this.element.empty();
        this._super();
    }
});