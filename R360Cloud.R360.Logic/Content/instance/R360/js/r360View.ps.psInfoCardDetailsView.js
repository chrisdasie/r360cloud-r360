/**
Displays ps model data details in the form of an information card <br>
<i>Sample:</i> </br><img style="width: 80%; margin-left: 10%;" src="../../img/doc/psInfoCardDetailsView.png" align="middle"/>

@class r360View.psInfoCardDetailsView
@extends radient360View.abstractView
*/
$.widget("r360View.psInfoCardDetailsView", $.radient360View.abstractView, /**@lends r360View.psInfoCardDetailsView*/ {
    options: {
        horizontalLayout: true
    },

    _create: function () {
        var that = this;
        this._super();
        if (this.options.horizontalLayout) {
            this.element.addClass('horizontal-layout');
        }
        var $wrapper = $('<div class="' + this.contextClass + '-wrapper"></div>');
        $wrapper.append('<div class="' + this.contextClass + '-kpi"></div>');
        $wrapper.append('<div class="glyphicon glyphicon-chevron-right ' + this.contextClass + '-hide"></div>');

        $wrapper.append('<div class="first-intent-table-wrapper"><table class="first-intent-table"><body><tr><td class="first-intent ' +
            this.contextClass + '-current"><div><div class="first-intent-value"></div><span class="first-intent-label">Current</span></div></div></td></tr>' +
            '<tr><td class="first-intent ' + this.contextClass +
            '-ytd"><div><div class="first-intent-value"></div><span class="first-intent-label">ytd</span></div></div></td></tr></body></table><div>');

        $wrapper.append('<div class="second-intent-table-wrapper"><table class="second-intent-table"><body><tr><td class="second-intent ' +
            this.contextClass + '-target"><div><span class="second-intent-value"></span><div class="second-intent-label">target</div></div></div></td>' +
            '<td class="second-intent ' + this.contextClass +
            '-type"><div><span class="second-intent-value"></span><div class="second-intent-label">type</div></div></div></td></tr></body></table><div>');

        this.element.append($wrapper);
        if (this.options.model) {
            this._initialize();
        }
        this.element.on("click", ".glyphicon-chevron-right", function () {
            that.options.model = $.object.extend({}, that.options.model);
            that.options.model.pointSelection = null;
            that.element.trigger("modelChange", that.options.model);
        });
    },

    _preSetOption: function (key, value) {
        var that = this;
        if (key == 'model') {
            return function () {
                if (that.options.model) {
                    that._initialize();
                }
            }
        }
    },

    _initialize: function () {
        var seriesIndex = this.options.model.pointSelection ? this.options.model.pointSelection.seriesIndex : -1;
        var pointIndex = this.options.model.pointSelection ? this.options.model.pointSelection.pointIndex : -1;        
        if (seriesIndex >= 0 && pointIndex >= 0) { // && this.options.model.series[seriesIndex].details) {
            var showKpi = this.options.model.series[seriesIndex].kpi;
            var value = Math.round(Math.random() * 1000) / 10;
            //value = "";
            //if (this.options.model.series[seriesIndex].details.current && this.options.model.series[seriesIndex].details.current[pointIndex]) {
            //    value = this.options.model.series[seriesIndex].details.current[pointIndex]
            //}
            this.element.find('.' + this.contextClass + '-current .first-intent-value').text(value);

            value = Math.round(Math.random() * 100) / 10;
            //value = "";
            //if (this.options.model.series[seriesIndex].details.ytd && this.options.model.series[seriesIndex].details.ytd[pointIndex]) {
            //    value = this.options.model.series[seriesIndex].details.ytd[pointIndex];
            //}
            this.element.find('.' + this.contextClass + '-ytd .first-intent-value').text(value);

            value = Math.round(Math.random() * 10) / 10;
            //value = "";
            //if (this.options.model.series[seriesIndex].details.target && this.options.model.series[seriesIndex].details.target[pointIndex]) {
            //    value = this.options.model.series[seriesIndex].details.target[pointIndex];
            //}
            this.element.find('.' + this.contextClass + '-target .second-intent-value').text(value);

            var types = ['', 'AR', 'C'];
            value = types[(Math.random() * 2 | 0) + 1];
            //value = "";
            //if (this.options.model.series[seriesIndex].details.type && this.options.model.series[seriesIndex].details.type[pointIndex]) {
            //    value = this.options.model.series[seriesIndex].details.type[pointIndex];
            //}
            this.element.find('.' + this.contextClass + '-type .second-intent-value').text(value);

            var kpiClasses = ['fa fa-circle status-critical', 'fa fa-circle status-non-critical', 'fa fa-circle status-good'];
            value = showKpi ? kpiClasses[Math.round(Math.random() * 3)] : '';
            //value = '';
            //if (this.options.model.series[seriesIndex].details.kpi && this.options.model.series[seriesIndex].kpi.status[pointIndex]) {
            //    value = 'status-' + this.options.model.series[seriesIndex].kpi.status[pointIndex];
            //}
            var $kpi = this.element.find('.' + this.contextClass + '-kpi');
            $kpi.removeClass('fa fa-circle status-critical status-non-critical status-good');
            $kpi.addClass(value);
        }
    },

    _destroy: function () {
        this.element.empty();
        this._super();
    }
});