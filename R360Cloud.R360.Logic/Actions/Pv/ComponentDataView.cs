﻿using R360CloudFramework.Logic.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R360Cloud.R360.Logic.Actions.Pv
{
    public class ComponentDataView : DataViewLinq
    {
        static List<dynamic> _records = null;
  
        public ComponentDataView()
        {
            // TODO: Calculate the correct url
            UrlPrefix = "api/data/s/pv/component/component_data";
        }

        public override dynamic GetMetadata()
        {
            return new { heroBoxes = GetMetadataheroBoxes(), actionButtons = GetMetadataActionButtons(), columns = GetMetadataColumns(), dataUrl = UrlPrefix };
        }

        public dynamic GetMetadataheroBoxes()
        {
            return new List<dynamic>();
        }

        public dynamic GetMetadataActionButtons()
        {
            return new List<dynamic>();
        }

        public dynamic GetMetadataColumns()
        {
            var columns = new List<dynamic>();
            columns.Add(new { data = "tagId", title = "Tag ID", sortable = true, visible = true, type = "text" });
            columns.Add(new
            {
                data = "type",
                title = "Type",
                sortable = true,
                visible = true,
                type = "multiselect",
                values = new string[] { "Circuit",
                "Valve",
                "Pipe",
                "Tank" }
            });
            columns.Add(new { data = "description", title = "Description", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "riskCode", title = "Risk Code", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "nextInspectionDate", title = "Next Inspection Date", sortable = true, visible = true });
            return columns;
        }

        public dynamic GetTemporaryClickThroughData(dynamic body)
        {
            return (body.startingData as List<dynamic>).Take(11);
        }

        public override IEnumerable<dynamic> GetStartingData()
        {
            var numRecordLoops = 10;

            if (_records == null)
            {
                _records = new List<dynamic>();

                System.Random rand = new System.Random();

                for (int i = 0; i < numRecordLoops; ++i)
                {
                    string id = GetRandomId(rand);
                    string type = GetRandomType(rand);
                    string description = GetRandomDescription(rand);
                    string riskCode = GetRandomRiskCode(rand);
                    string nextInspectionDate = GetRandomNextInspectionDate(rand);

                    _records.Add(new { tagId = string.Format("<a target='_blank' href='#PV_VESSEL'>{0}</a>", id),
                                       type = type,
                                       description = description,
                                       riskCode = riskCode,
                                       nextInspectionDate = nextInspectionDate });
                }
            }

            return _records;
        }

        public override TypeInformation GetTypeInformation()
        {
            return new ComponentTypeInformation();
        }

        private string GetRandomId(System.Random rand)
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 4; i++)
            {
                sb.Append(allowedChars[rand.Next(0, allowedChars.Length)]);
            }
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            return sb.ToString();
        }

        private string GetRandomType(System.Random rand)
        {
            string[] types = 
            {
                "Circuit",
                "Valve",
                "Pipe",
                "Tank"
            };
            return types[rand.Next(0, types.Length)];
        }

        private string GetRandomDescription(System.Random rand)
        {
            string[] descriptions = 
            {
                "Seawater Circuit A",
                "Seawater Circuit B",
                "Deluge System A",
                "Deluge System B",
                "Firewater System A",
                "Firewater System B",
                "Hydrocarbon System A",
                "Hydrocarbon System B"
            };
            return descriptions[rand.Next(0, descriptions.Length)];
        }

        private string GetRandomRiskCode(System.Random rand)
        {
            string[] codes = 
            {
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
            };
            return codes[rand.Next(0, codes.Length)];
        }

        private string GetRandomNextInspectionDate(System.Random rand)
        {
            return DateTime.Now.AddDays(rand.Next(999)).ToShortDateString();
        }

        class ComponentTypeInformation : TypeInformation
        {
            public bool IsString(string key)
            {
                return key != "type" ? true : false;
            }

            public bool IsBoolean(string key)
            {
                return false;
            }

            public bool IsMultiString(string key)
            {
                return key == "type" ? true : false;
            }
        }
    }
}