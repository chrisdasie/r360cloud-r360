﻿using NLog;
using R360CloudFramework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R360Cloud.R360.Logic.Actions.Pv
{
    public class FlagTable
    {
        private static List<string> _flags = new List<string>() { "Create work order for failed valve",
            "Monitor thickness levels weekly.", "Review inspection results for deluge systems." };

        public dynamic GetData()
        {
            return new { body = GetBody() };
        }

        private dynamic GetBody()
        {
            var rows = new List<dynamic>();
            for(int i = 0; i < 3; i++)
            {
                rows.Add(GetRow(i));
            }
            return new { rows = rows };
        }

        private static dynamic GetRow(int rowIndex)
        {
            var cells = new List<dynamic>();
            cells.Add(new { type = "labelCell", options = new { title = rowIndex + 1 } });
            cells.Add(new { type = "labelCell", options = new { title = _flags[rowIndex] } });
            cells.Add(new { type = "controlIcon", options = new { glyphicon = "ok-circle" } });
            return new { cells = cells };
        }
    }
}