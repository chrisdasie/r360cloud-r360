﻿using NLog;
using R360CloudFramework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R360Cloud.R360.Logic.Actions.Pv
{
    public class CircuitInformation
    {
        static string[] ThreatDescriptions = new string[] { "Exposure of 80% of circuit to seawater leaves high potential of seawater corrosion.",
        "Weld points under strain"};
        
        static string[] MonitoringTags = new string[] { "Review Thickness Monitoring levels at least monthly.",  
            "Inspection weld points every 6 months." };

        static string[] MonitoringTemps = new string[] { "100", "125", "150", "180" };

        static string[] Inspections = new string[] { "Check for corrosion on port side where exposure to seawater is greatest",  
            "Inspection of weld points", "Continuous review of Thickness Monitoring levels." };

        public dynamic GetDegradationDataTableWithDetails()
        {
            return new { 
                table = new { header = GetTableHeader(), body = GetTableBody(), cssClass = "degradation-table" },
                details = new { metadata = GetDetailsMetadata(), data = GetDetailsData() }
            };
        }

        private dynamic GetTableHeader()
        {
            var rows = new List<dynamic>();
            var cells = new List<dynamic>() { 
                new { type = "labelCell", options = new { title = "Degradation System Threats", description = "" } },
                new { type = "labelCell", options = new { title = "Likelihood of Failure", description = "" } } };
            rows.Add(new { cells = cells });
            return new { rows = rows };
        }

        private dynamic GetTableBody()
        {
            var rows = new List<dynamic>();

            var cells = new List<dynamic>() { 
                new { type = "labelCell", options = new { title = "Corrosion" } },
                new { type = "labelCell", options = new { title = "3" }, cssClass = "center-align-text" } };
            rows.Add(new { cells = cells, cssClass = "active" });

            cells = new List<dynamic>() { 
                new { type = "labelCell", options = new { title = "Galvanic Weld" } },
                new { type = "labelCell", options = new { title = "3" }, cssClass = "center-align-text" } };
            rows.Add(new { cells = cells });

            return new { rows = rows };
        }

        private dynamic GetDetailsMetadata()
        {
            return new
            {
                attributes = new[] {
                new { name = "threatDescription", title = "Threat Description" },
                new { name = "monitoringTags", title = "Monitoring Tags" },
                new { name = "monitoringTemp", title = "Monitoring Temp Avg" },
                new { name = "inspection", title = "Inspection" } }
            };
        }

        private dynamic GetDetailsData()
        {
            return new [] { 
                new { threatDescription = ThreatDescriptions[0], monitoringTags = MonitoringTags[0], monitoringTemp = MonitoringTemps[0], inspection = Inspections[0] },
                new { threatDescription = ThreatDescriptions[1], monitoringTags = MonitoringTags[1], monitoringTemp = MonitoringTemps[1], inspection = Inspections[1] }};
        }
    }
}