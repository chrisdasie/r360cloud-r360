﻿using NLog;
using R360CloudFramework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R360Cloud.R360.Logic.Actions.Pv
{
    public class KnownTable
    {
        private static List<string> _knowns = new List<string>() { "Deluge System A Valve 3 under repair, awaiting new valve.", 
            "Hydrocarbon System B thickness monitoring equipment temporarily out of service, manual checks required until servicing complete.",
            "Corrosion rates in lower hull have been steadily increasing, monitoring frequency increased as a result."};

        public dynamic GetData()
        {
            return new { body = GetBody() };
        }

        private dynamic GetBody()
        {
            var rows = new List<dynamic>();
            for (int i = 0; i < 3; i++)
            {
                rows.Add(GetRow(i));
            }
            return new { rows = rows };
        }

        private static dynamic GetRow(int rowIndex)
        {
            var cells = new List<dynamic>();
            cells.Add(new { type = "labelCell", options = new { title = rowIndex + 1 } });
            cells.Add(new { type = "labelCell", options = new { title = _knowns[rowIndex] } });
            return new { cells = cells };
        }
    }
}