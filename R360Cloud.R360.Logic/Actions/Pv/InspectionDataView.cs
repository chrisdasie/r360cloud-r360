﻿using R360CloudFramework.Logic.Actions;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R360Cloud.R360.Logic.Actions.Pv
{
    public class InspectionDataView : DataViewLinq
    {
        static List<dynamic> _records = null;

        public InspectionDataView()
        {
            // TODO: Calculate the correct url
            UrlPrefix = "api/data/s/pv/inspection/inspection_data";
        }

        public override dynamic GetMetadata()
        {
            return new { heroBoxes = GetMetadataheroBoxes(), actionButtons = GetMetadataActionButtons(), columns = GetMetadataColumns(), dataUrl = UrlPrefix };
        }

        public dynamic GetMetadataheroBoxes()
        {
            return new List<dynamic>();
        }

        public dynamic GetMetadataActionButtons()
        {
            return new List<dynamic>();
        }

        public dynamic GetMetadataColumns()
        {
            var columns = new List<dynamic>();
            columns.Add(new { data = "inspectionDate", title = "Inspection Date", sortable = true, visible = true });
            columns.Add(new { data = "type", title = "Type", sortable = true, visible = true, type = "multiselect", values = new string[] { "Internal", "External" } });
            columns.Add(new { data = "reportNumber", title = "Report Number", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "result", title = "Result", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "fileName", title = "File Name", sortable = false, visible = false, type = "text" });
            return columns;
        }

        public dynamic GetTemporaryRiskAnalysisDataClickThrough(dynamic body)
        {
            return (body.startingData as List<dynamic>).Take(11);
        }

        public override IEnumerable<dynamic> GetStartingData()
        {
            var numRecordLoops = 4;

            if (_records == null)
            {
                _records = new List<dynamic>();

                System.Random rand = new System.Random();

                for (int i = 0; i < numRecordLoops; ++i)
                {
                    string reportNumber = GetRandomReportNumber(rand);
                    string type = GetRandomType(rand);
                    string result = GetRandomResult(rand);
                    string fileName = GetRandomFileName(rand);
                    string inspectionDate = GetRandomNextInspectionDate(rand);

                    _records.Add(new
                    {
                        inspectionDate = inspectionDate,
                        type = type,
                        reportNumber = reportNumber,
                        result = result,
                        fileName = fileName
                    });
                }
            }

            return _records;
        }

        private string GetRandomReportNumber(System.Random rand)
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 2; i++)
            {
                sb.Append(allowedChars[rand.Next(0, allowedChars.Length)]);
            }
            sb.Append("-").AppendFormat("{0:D2}", rand.Next(99));
            sb.Append("-").AppendFormat("{0:D3}", rand.Next(999));
            return sb.ToString();
        }

        private string GetRandomFileName(System.Random rand)
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 4; i++)
            {
                sb.Append(allowedChars[rand.Next(0, allowedChars.Length)]);
            }
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            sb.Append("-").AppendFormat("{0:D4}", rand.Next(9999));
            sb.Append(".pdf");
            return sb.ToString();
        }

        private string GetRandomNextInspectionDate(System.Random rand)
        {
            return System.DateTime.Now.AddDays(rand.Next(999)).ToShortDateString();
        }

        private string GetRandomType(System.Random rand)
        {
            string[] types = 
            {
                "Internal",
                "External",
                "Unscheduled"
            };
            return types[rand.Next(0, types.Length)];
        }

        private string GetRandomResult(System.Random rand)
        {
            string[] results = 
            {
                "Pass",
                "Fail",
                "Incomplete"
            };
            return results[rand.Next(0, results.Length)];
        }

        public override TypeInformation GetTypeInformation()
        {
            return new ComponentTypeInformation();
        }

        class ComponentTypeInformation : TypeInformation
        {
            public bool IsString(string key)
            {
                return key != "type" ? true : false;
            }

            public bool IsBoolean(string key)
            {
                return false;
            }

            public bool IsMultiString(string key)
            {
                return key == "type" ? true : false;
            }
        }
    }
}