﻿using NLog;
using R360CloudFramework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R360Cloud.R360.Logic.Actions.Sample
{
    public class SampleTable
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        static CypherRunner cypherRunner = new CypherRunner();

        public dynamic GetData()
        {
            return new { header = GetHeader(), body = GetBody() };
        }

        private dynamic GetHeader()
        {
            var rows = new List<dynamic>();
            var cells = new List<dynamic>();
            for (int i = 0; i < 5; i++)
            {
                cells.Add(new { type = "labelCell", options = new { title = "header " + i, description = "description which may be very long " + i } });
            }
            rows.Add(new { cells = cells });
            return new {rows = rows };
        }

        private dynamic GetBody()
        {
            var rows = new List<dynamic>();
            var r = new Random();
            for (int i = 0; i < 5; i++)
            {
                var cells = new List<dynamic>();
                cells.Add(new { type = "imageCell", options = new { title = i * 10 + "", description = "click here to navigate to a magical place", src = "http://upload.wikimedia.org/wikipedia/commons/8/87/Symbol_thumbs_up.svg", height = "2em", clazz = "asdf" } });
                for (int j = 0; j < 2; j++)
                {
                    cells.Add(new { type = "labelCell", options = new { title = "value " + i + "," + j, description = "description which may be very long " + i + "," + j } });
                }
                cells.Add(new { type = "sparklineCell", options = new { title = "", sparklineOptions = new { model = new { series = new[] { new { data = new[] { r.Next(10), r.Next(10), r.Next(10), r.Next(10), r.Next(10), r.Next(10) } } } } } } });
                cells.Add(new { type = "linkCell", options = new { title = i * 10 + "", description = "click here to navigate to a magical place", panelType = "rawDataPanel", panelOptions = new { title = "RawData for " + i, metadataUrl = "api/data/s/psv/certificate/certificate_metadata" } } });
                rows.Add(new { cells = cells });
            }
            return new { rows = rows };
        }

    }
}