﻿using Newtonsoft.Json.Linq;
using NLog;
using R360CloudFramework.Data;
using R360CloudFramework.Data.Extensions;
using R360CloudFramework.Logic.Actions;
using R360CloudFramework.Logic.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace R360Cloud.R360.Logic.Actions.Sample
{
    public class SampleDataView : DataViewCypher
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        static CypherRunner cypherRunner = new CypherRunner();

        public SampleDataView()
        {
            // TODO: Calculate the correct url
            UrlPrefix = "api/data/s/sample/sample/sample_data";
        }

        public override dynamic GetMetadata()
        {
            return new { heroBoxes = GetMetadataheroBoxes(), actionButtons = GetMetadataActionButtons(), columns = GetMetadataColumns(), dataUrl = UrlPrefix };
        }

        public dynamic GetMetadataheroBoxes()
        {
            var heroBoxes = new List<dynamic>();
            dynamic counts = GetCounts();

            heroBoxes.Add(new { primary = counts["allCount"], secondary = "100%", tertiary = "All Certificates", description = "Click to clear the filter and show all certificates" });
            heroBoxes.Add(new { primary = counts["completeCount"], secondary = "100%", tertiary = "Completed Certificates", description = "Click to filter and show only completed certificates" });
            heroBoxes.Add(new { primary = counts["requiresRbiCount"], secondary = "2%", tertiary = "Devices Requiring RBI", description = "Click to filter and show only devices requiring RBI updates" });
            heroBoxes.Add(new { primary = counts["inProgressCount"], secondary = "2%", tertiary = "In Progress Certificates", description = "Click to filter and show only in progress certificates" });
            return heroBoxes;
        }

        public dynamic GetMetadataActionButtons()
        {
            var actionButtons = new List<dynamic>();
            actionButtons.Add(new { title = "Job Card", description = "Some descriptive text about what this button does", iconClass = "fa fa-plus-circle" });
            actionButtons.Add(new { title = "Signed Certificate", description = "Some descriptive text about what this button does", iconClass = "fa fa-arrow-circle-o-up" });
            return actionButtons;
        }

        public dynamic GetMetadataColumns()
        {
            var columns = new List<dynamic>();
            columns.Add(new { data = "deviceId", title = "Device ID", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "type", title = "Type", sortable = true, visible = true, type = "multiselect", values = new string[] { "PrePop", "Offshore Cert", "COI", "Recert"} });
            columns.Add(new { data = "inspectionDate", title = "Inspection Date", sortable = true, visible = true });
            columns.Add(new { data = "serialNumber", title = "Serial Number", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "reportNumber", title = "Report Number", sortable = true, visible = true, type = "text" });
            columns.Add(new { data = "status", title = "Status", sortable = true, visible = true, type = "multiselect", values = new string[] { "In Progress", "Upload Complete" } });
            return columns;
        }

        public override dynamic GetData(long filter, long sort, long start, long length, string format)
        {
            dynamic filterAndSort = GetFilterAndSort(filter, sort);

            Tuple<string, dynamic> master = null;
            if (filterAndSort.filter.masterFilter != null)
            {
                var masterFilter = filterAndSort.filter.masterFilter;
                if (masterFilter.name != null) {
                    var r = new RoutingManager();
                    master = r.ExecuteSpecialRequest((string)masterFilter.name, masterFilter);
                }
            }
            var query = new StringBuilder();

            if (master != null)
            {
                query.Append(master.Item1);
            }
            query.Append("MATCH (c:psv_cert) WHERE NOT(c.reportNumber =~ 'R360-G-.*') ");
            if (master != null)
            {
                query.Append("AND id(c) = id ");
            }

            query.Append(
@"OPTIONAL MATCH (c)-[:certifies]-(d:psv_device) 
OPTIONAL MATCH (c)-[:certifies]-(p:psv_psv)
OPTIONAL MATCH (c)-[:has]-(f:file)
OPTIONAL MATCH (c)<-[:derived_from]-(cl:psv_rbi_user_cl_input) 
WITH 
c.reportNumber AS reportNumber, c.date AS inspectionDate, 
CASE WHEN c:psv_offshorecert THEN 'Offshore Cert' WHEN c:psv_prepopcert THEN 'PrePop' WHEN c:psv_coi THEN 'COI' WHEN c:psv_recert THEN 'Recert' ELSE null END AS type, 
CASE WHEN c:psv_newcert THEN 'In Progress' ELSE 'Upload Complete' END AS status, 
MAX(d.deviceId) AS deviceId, MAX(p.serialNumber) AS serialNumber, MAX(f.md5) AS fileName, COUNT(cl) AS clCount 
WITH reportNumber, inspectionDate, type, status, deviceId, serialNumber, fileName, clCount > 0 AS hasCl ");

            var orderBy = new StringBuilder();
            orderBy.Append("ORDER BY ").Append(filterAndSort.sort[0].property.ToObject<string>());
            orderBy.Append(filterAndSort.sort[0].direction.ToObject<string>().Equals("desc") ? " DESC " : " ");

            query.Append(orderBy);

            var stringProperties = new string[] { "reportNumber", "deviceId", "serialNumber" };
            var integerProperties = new string[] { };
            var booleanProperties = new string[] { };
            var multiselectStringProperties = new string[] { "type", "status" };
            var multiselectIntegerProperties = new string[] { };

            var slaveParameters = AddFilter(filterAndSort, query, stringProperties, integerProperties, booleanProperties, multiselectStringProperties, multiselectIntegerProperties);

            var parameters = new Dictionary<string, dynamic>();
            if (master != null)
            {
                parameters.Add("master", master.Item2);
            }
            parameters.Add("slave", slaveParameters);
            var parametersQuery = new Dictionary<string, dynamic>(parameters);
            parametersQuery.Add("skip", start);
            parametersQuery.Add("limit", length);

            var records = cypherRunner.Execute(query.ToString() + "RETURN * " + /*orderBy.ToString() +*/ "SKIP {skip} LIMIT {limit} ", parametersQuery);
            foreach (var record in records)
            {
                var id = record["deviceId"];
                //UrlHelper Url = new UrlHelper(HttpContext.Current.Items["MS_HttpRequestMessage"] as HttpRequestMessage);
                if (format.Equals("SUMMARY"))
                {
                    //record.Remove("deviceId");
                    //record.Add("deviceId", "<a target='_blank' href='" + Url.Link("Default", new { Controller = "PSV", Action = "PsvInformation", deviceId = id }) + "'>" + id + "</a>");
                }
                else
                {
                    //record.Add("deviceIdLink", "<a target='_blank' href='" + Url.Link("Default", new { Controller = "PSV", Action = "PsvInformation", deviceId = id }) + "'>" + id + "</a>");
                }
            }

            var cypherFilteredCountResult = cypherRunner.Execute(query.ToString() + "RETURN COUNT(*) AS Count", parameters);
            var totalFiltered = cypherFilteredCountResult[0]["Count"].ToObject<int>();

            var totalCountQuery = master == null ? "MATCH (c:psv_cert) WHERE NOT(c.reportNumber =~ 'R360-G-.*') RETURN COUNT(*) AS Count" : (master.Item1 + "RETURN COUNT(*) AS Count");
            var cypherTotalCountResult = cypherRunner.Execute(totalCountQuery, parameters);
            var total = cypherTotalCountResult[0]["Count"].ToObject<int>();

            return new { total = total, totalFiltered = totalFiltered, records = records };
        }

        private static Dictionary<string, dynamic> AddFilter(dynamic filterAndSort, StringBuilder query, string[] stringProperties, string[] integerProperties, string[] booleanProperties, string[] multiselectStringProperties, string[] multiselectIntegerProperties)
        {
            var first = true;
            var slaveParameters = new Dictionary<string, dynamic>();
            if (filterAndSort.filter != null && filterAndSort.filter.general != null)
            {
                var dateRange = (JToken)filterAndSort.filter.general.dateRange;
                if (dateRange != null)
                {
                    var startJ = dateRange.SelectToken("start");
                    var endJ = dateRange.SelectToken("end");
                    if (startJ != null && endJ == null)
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append("(inspectionDate >= {slave}.start) ");
                        slaveParameters.Add("start", startJ.ToObject<DateTime>().ToUnixTime());
                    }
                    if (startJ == null && endJ != null)
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append("(inspectionDate < {slave}.end ) ");
                        slaveParameters.Add("end", endJ.ToObject<DateTime>().ToUnixTime());
                    }
                    if (startJ != null && endJ != null)
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append("(inspectionDate >= {slave}.start AND inspectionDate < {slave}.end) ");
                        slaveParameters.Add("start", startJ.ToObject<DateTime>().ToUnixTime());
                        slaveParameters.Add("end", endJ.ToObject<DateTime>().ToUnixTime());
                    }
                }
            }
            var property = (JToken)filterAndSort.filter.property;
            if (property != null)
            {
                var map = property.ToObject<Dictionary<string, dynamic>>();
                foreach (var key in map.Keys)
                {
                    if (stringProperties.Contains(key))
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append(key).Append("=~ {slave}.").Append(key).Append(" ");
                        slaveParameters.Add(key, "(?si).*" + Regex.Escape(map[key]) + ".*");
                    }
                    else if (integerProperties.Contains(key))
                    {
                        try
                        {
                            var value = Convert.ToInt32(map[key]);
                            query.Append(first ? "WHERE " : "AND ");
                            first = false;
                            query.Append(key).Append(" = {slave}.").Append(key).Append(" ");
                            slaveParameters.Add(key, value);
                        }
                        catch (FormatException)
                        {
                            // This is probably ok.
                        }
                    }
                    else if (multiselectStringProperties.Contains(key))
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append(key).Append(" =~ {slave}.").Append(key).Append(" ");
                        slaveParameters.Add(key, ((string)map[key]).Replace("Blank", ""));
                    }
                    else if (multiselectIntegerProperties.Contains(key))
                    {
                        query.Append(first ? "WHERE " : "AND ");
                        first = false;
                        query.Append("str(").Append(key).Append(") =~ {slave}.").Append(key).Append(" ");
                        slaveParameters.Add(key, ((string)map[key]).Replace("Blank", ""));
                    }
                    else if (booleanProperties.Contains(key))
                    {
                        try
                        {
                            var value = Convert.ToBoolean(map[key]);
                            query.Append(first ? "WHERE " : "AND ");
                            first = false;
                            query.Append(key).Append("= {slave}.").Append(key).Append(" ");
                            slaveParameters.Add(key, value);
                        }
                        catch (FormatException)
                        {
                            // This is probably ok.
                        }
                    }

                }
            }
            return slaveParameters;
        }

        public SpecialActionResult Filter1(dynamic masterFilter)
        {
            var query = "MATCH (c:psv_cert)-[:has]->(:file) " +
                        "WHERE (c:psv_prepopcert OR c:psv_offshorecert) AND NOT ((c)<-[:derived_from]-(:psv_rbi_user_cl_input)) " +
                        "WITH id(c) AS id ";
            return new SpecialActionResult() { Body = new Tuple<string, dynamic>(query, new { }) };
        }

        public SpecialActionResult Filter2(dynamic masterFilter)
        {
            var query = "MATCH (c:psv_newcert) " +
                        "WITH id(c) AS id ";
            return new SpecialActionResult() { Body = new Tuple<string, dynamic>(query, new { }) };
        }

        public SpecialActionResult Filter3(dynamic masterFilter)
        {
            var query = "MATCH (c:psv_cert) " +
                        "WHERE NOT (c:psv_newcert) " +
                        "WITH id(c) AS id ";
            return new SpecialActionResult() { Body = new Tuple<string, dynamic>(query, new { }) };
        }

        public SpecialActionResult Filter4(dynamic masterFilter)
        {
            var query = "MATCH (c:psv_cert) " +
                        "WHERE NOT (c:psv_newcert) " +
                        "WITH id(c) AS id ";
            return new SpecialActionResult() { Body = new Tuple<string, dynamic>(query, new { }) };
        }

        public SpecialActionResult Filter5(dynamic masterFilter)
        {
            var query = "MATCH (c:psv_cert) " +
                        "WHERE NOT (c:psv_newcert) " +
                        "WITH id(c) AS id ";
            return new SpecialActionResult() { Body = new Tuple<string, dynamic>(query, new { }) };
        }

        public dynamic GetCounts()
        {
            var cypher = @"OPTIONAL MATCH (all:psv_cert) WHERE NOT(all.reportNumber =~ 'R360-G-.*') 
WITH COUNT(all) AS allCount
OPTIONAL MATCH (inProgress:psv_newcert) WHERE NOT(inProgress.reportNumber =~ 'R360-G-.*')
WITH allCount, COUNT(inProgress) AS inProgressCount
OPTIONAL MATCH (complete:psv_cert) WHERE NOT(complete.reportNumber =~ 'R360-G-.*') AND NOT(complete:psv_newcert)
WITH allCount, inProgressCount, COUNT(complete) AS completeCount
OPTIONAL MATCH (requiresRbi:psv_cert) WHERE NOT(requiresRbi.reportNumber =~ 'R360-G-.*') AND (requiresRbi:psv_prepopcert OR requiresRbi:psv_offshorecert) AND NOT((requiresRbi)<-[:derived_from]-(:psv_rbi_user_cl_input)) 
RETURN allCount, inProgressCount, completeCount, COUNT(requiresRbi) AS requiresRbiCount";
            var cypherResult = cypherRunner.Execute(cypher, null);
            return cypherResult[0];
        }

    }
}