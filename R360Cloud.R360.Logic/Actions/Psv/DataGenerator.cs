﻿using R360CloudFramework.Data;
using R360CloudFramework.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R360Cloud.Husky.Logic.Actions.Psv
{
    class DataGenerator
    {
        private static CypherRunner cypherRunner = new CypherRunner();
        private static Random random = new Random(0);

        public dynamic Generate(long deviceCount, long certCount)
        {
            var historyYears = certCount; // TODO: Fix routing!
            var start = DateTime.Now;
            var devices = new Dictionary<string, Dictionary<string, dynamic>>();
            var certs = new Dictionary<string, List<Dictionary<string, dynamic>>>();
            var intervals = new Dictionary<string, List<Dictionary<string, dynamic>>>();

            for (int i = 0; i < deviceCount; i++)
            {
                var device = new Dictionary<string, dynamic>();
                var deviceId = GenerateIdentifier(i, "PSV-");
                // Attributes with meaning.
                device.Add("deviceId", deviceId);
                device.Add("commissionDate", DateTime.Now.AddYears((int)-historyYears).ToUnixTime());
                device.Add("swapOut", i % 2 != 0);
                device.Add("hull", i % 7 == 0);
                device.Add("boiler", i % 63);
                // Attributes with randon values.
                device.Add("designType", GetRandomDesignType());
                device.Add("plantArea", GetRandomPlantArea());
                device.Add("protectedEquipmentId", GenerateIdentifier(0));
                device.Add("location", GetRandomLocation());
                device.Add("reliefSetPressure", GetRandomReliefSetPressure());
                device.Add("system", GetRandomSystem());
                device.Add("systemCondition", GetRandomSystemCondition());
                device.Add("serviceCategory", GetRandomServiceCategory());
                device.Add("subSystem", GetRandomSubSystem());
                device.Add("componentType", GetRandomComponentType());
                device.Add("pAndId", GetRandomPAndId());
                device.Add("serviceFluid", GetRandomServiceFluid());
                device.Add("protectedEquipmentTagNumber", GetRandomProtectedEquipmentTagNumber());
                device.Add("coldDifferentialTestPressure", GetRandomReliefSetPressure());
                device.Add("coldDifferentialTestPressureUnit", GetRandomColdDifferentialTestPressureUnit());
                devices.Add(deviceId, device);
                certs.Add(deviceId, new List<Dictionary<string, dynamic>>());
                intervals.Add(deviceId, new List<Dictionary<string, dynamic>>());
                if (i % 2 == 0)
                {
                    // Offshore
                    for (int j = 0; j < historyYears; j++)
                    {
                        var offset = new TimeSpan(-random.Next(364), 0, 0, 0);
                        var cert = new Dictionary<string, dynamic>();
                        var reportNumber = GenerateIdentifier(i, "CERT-");
                        cert.Add("reportNumber", reportNumber);
                        cert.Add("date", start.AddYears(-j).Add(offset).ToUnixTime());
                        cert.Add("certificateType", "OffshoreCert");
                        cert.Add("inspectionResult", GetRandomInspectionResult());
                        // TODO: Add all the other attributes.
                        certs[deviceId].Add(cert);


                        var interval = new Dictionary<string, dynamic>();
                        interval.Add("startDate", start.AddYears(-j).ToUnixTime());
                        interval.Add("endDate", start.AddYears(-j + 1).ToUnixTime());
                        interval.Add("date", start.AddYears(-j).AddDays(30).Add(offset).ToUnixTime());
                        var ui = 12 + random.Next(6) * 12;
                        var si = 12 + random.Next(6) * 12;
                        interval.Add("ui", ui);
                        interval.Add("si", si);
                        interval.Add("i", Math.Min(ui, si));
                        intervals[deviceId].Add(interval);
                    }
                }
                else
                {
                    // Swapout
                    for (int j = 0; j < historyYears; j++)
                    {
                        var offset = new TimeSpan(-random.Next(349), 0, 0, 0);
                        {
                            var cert = new Dictionary<string, dynamic>();
                            var reportNumber = GenerateIdentifier(i, "CERT-");
                            cert.Add("reportNumber", reportNumber);
                            cert.Add("date", start.AddYears(-j).AddDays(-15).Add(offset).ToUnixTime());
                            cert.Add("certificateType", "Recert");
                            // TODO: Add all the other attributes.
                            certs[deviceId].Add(cert);
                        }
                        {
                            var cert = new Dictionary<string, dynamic>();
                            var reportNumber = GenerateIdentifier(i, "CERT-");
                            cert.Add("reportNumber", reportNumber);
                            cert.Add("date", start.AddYears(-j).Add(offset).ToUnixTime());
                            cert.Add("certificateType", "PrePop");
                            cert.Add("inspectionResult", GetRandomInspectionResult());
                            // TODO: Add all the other attributes.
                            certs[deviceId].Add(cert);
                        }
                        {
                            var interval = new Dictionary<string, dynamic>();
                            interval.Add("startDate", start.AddYears(-j).ToUnixTime());
                            interval.Add("endDate", start.AddYears(-j + 1).ToUnixTime());
                            interval.Add("date", start.AddYears(-j).AddDays(-20).Add(offset).ToUnixTime());
                            var ui = 12 + random.Next(6) * 12;
                            var si = 12 + random.Next(6) * 12;
                            interval.Add("ui", ui);
                            interval.Add("si", si);
                            interval.Add("i", Math.Min(ui, si));
                            intervals[deviceId].Add(interval);
                        }
                    }
                }
            }
            
            // Insert all the devices.
            var deviceCypher = @"
CREATE (d:psv_device {props}) WITH d 
CREATE (d)-[:attribute {date: {startDate}}]->(:psv_rbi_user_lof_input {lof: 1 + ROUND(RAND() * 4), date: {startDate}}) 
CREATE (d)-[:attribute {date: {startDate}}]->(:psv_rbi_user_cof_input {cof: 1 + ROUND(RAND() * 4), date: {startDate}})";
            cypherRunner.ExecuteRaw(deviceCypher, new {startDate = start.AddYears((int)-historyYears).ToUnixTime(), props = devices.Values});

            // Insert all the certs
            foreach (var deviceId in certs.Keys)
            {
                var certCypher = @"
CREATE (c:psv_cert {props}) WITH c 
MATCH (d:psv_device {deviceId: {deviceId}}) CREATE (c)-[:certifies]->(d)";
                cypherRunner.ExecuteRaw(certCypher, new { deviceId = deviceId, props = certs[deviceId] });
            }
            
            // Label all the certs
            {
                var labelCypher = @"
MATCH (c:psv_cert {certificateType: 'OffshoreCert'}) SET c:psv_offshorecert WITH COUNT(*) AS junk
MATCH (c:psv_cert {certificateType: 'Recert'}) SET c:psv_recert WITH COUNT(*) AS junk
MATCH (c:psv_cert {certificateType: 'PrePop'}) SET c:psv_prepopcert WITH COUNT(*) AS junk
MERGE (:psv_rbi_aggregate_calculation {class:'OpsDashboard.Psv.AggregateCalculation2013'})
MERGE (:psv_rbi_user_input_calculation {class:'OpsDashboard.Psv.UserInputCalculation2013'})
MERGE (:psv_rbi_static_input_calculation {class:'OpsDashboard.Psv.StaticInputCalculation2013'})
MERGE (:psv_system_condition {code: 'W', title: 'Certifying Authority Witness'})
MERGE (:psv_system_condition {code: 'F', title: 'Gas Process Shutdown'})
MERGE (:psv_system_condition {code: 'N', title: 'Unit Shutdown'})
MERGE (:psv_system_condition {code: 'J', title: 'G.C. Train A Shutdown'})
MERGE (:psv_system_condition {code: 'Q', title: 'System Outage'})
MERGE (:psv_system_condition {code: 'M', title: 'Main Parent Outage'})
MERGE (:psv_system_condition {code: 'K', title: 'G.C. Train B Shutdown'})
MERGE (:psv_system_condition {code: 'I', title: 'Item Outage'})
MERGE (:psv_system_condition {code: 'H', title: 'Production Shutdown'}) WITH COUNT(*) AS junk
MATCH (d:psv_device) MATCH (sc:psv_system_condition {code:d.systemCondition}) MERGE (d)-[:attribute]-(sc) WITH COUNT(*) AS junk
MATCH (d:psv_device) MERGE (s:system {name:d.system, description: d.system}) MERGE (d)-[:attribute]-(s)
";
                cypherRunner.ExecuteRaw(labelCypher, null);
            }

            foreach (var deviceId in intervals.Keys)
            {
                intervals[deviceId].Reverse();
                foreach (var interval in intervals[deviceId])
                {
                    var intervalCypher = @"
MATCH 
(ac:psv_rbi_aggregate_calculation),
(uic:psv_rbi_user_input_calculation),
(sic:psv_rbi_static_input_calculation),
(d:psv_device {deviceId: {deviceId}}),
(d)-[:attribute]->(lof:psv_rbi_user_lof_input),
(d)-[:attribute]->(cof:psv_rbi_user_cof_input),
(d)<-[:certifies]-(cert:psv_cert) WHERE (cert:psv_offshorecert OR cert:psv_prepopcert) AND cert.date >= {startDate} AND cert.date < {endDate}
CREATE 
(d)-[:attribute {date:{date}}]->(cl:psv_rbi_user_cl_input {cl: 1 + ROUND(RAND() * 3), date: {date}}),
(ui:psv_interval {months: {ui}}),(ui)-[:derived_from]->(uic),(ui)-[:derived_from]->(lof),(ui)-[:derived_from]->(cof),(ui)-[:derived_from]->(cl),
(si:psv_interval {months: {si}}),(si)-[:derived_from]->(sic),(si)-[:derived_from]->(d),
(i:psv_interval {months: {i}}),(i)-[:derived_from]->(ac),(i)-[:derived_from]->(ui),(i)-[:derived_from]->(si),(i)-[:as_of]->(cert)
WITH d, cl MATCH
(d)-[r:attribute]->(old_cl:psv_rbi_user_cl_input) WHERE NOT((old_cl)<-[:revition_of]-()) AND old_cl <> cl
//DELETE r
CREATE (cl)-[:revision_of]->(old_cl)
";

                    cypherRunner.ExecuteRaw(intervalCypher, new { deviceId = deviceId, startDate = interval["startDate"], endDate = interval["endDate"], date = interval["date"], ui = interval["ui"], si = interval["si"], i = interval["i"] });
                }
            }

            return (DateTime.Now - start).TotalMilliseconds;

        }

        static List<string> systemConditions;
        private dynamic GetRandomSystemCondition()
        {
            if (systemConditions == null) {
                systemConditions = new List<string>();
                systemConditions.Add("W");
                systemConditions.Add("F");
                systemConditions.Add("N");
                systemConditions.Add("J");
                systemConditions.Add("Q");
                systemConditions.Add("M");
                systemConditions.Add("K");
                systemConditions.Add("I");
                systemConditions.Add("H");
            }
            return random.Next(100) < 90 ? "" : GetRandom2(systemConditions);
        }

        static List<string> inspectionResults;
        private dynamic GetRandomInspectionResult()
        {
            if (inspectionResults == null)
            {
                inspectionResults = new List<string>();
                inspectionResults.Add("pass");
                inspectionResults.Add("fail");
                inspectionResults.Add("criticalFail");
            }
            var normal = random.NextDouble();
            var index = normal < 0.9 ? 0 : normal < 0.99 ? 1 : 2;
            return inspectionResults[index];
        }

        static List<string> coldDifferentialTestPressureUnits;
        private dynamic GetRandomColdDifferentialTestPressureUnit()
        {
            if (coldDifferentialTestPressureUnits == null)
            {
                coldDifferentialTestPressureUnits = new List<string>();
                coldDifferentialTestPressureUnits.Add("BARG");
                coldDifferentialTestPressureUnits.Add("KPAG");
                coldDifferentialTestPressureUnits.Add("PSIG");
            }
            return GetRandom2(coldDifferentialTestPressureUnits);
        }

        private dynamic GetRandomProtectedEquipmentTagNumber()
        {
            return (20 + random.Next(10)) + "-V-2" + (100 + random.Next(100));
        }

        static List<string> serviceFluids;
        private dynamic GetRandomServiceFluid()
        {
            if (serviceFluids == null)
            {
                serviceFluids = new List<string>();
                serviceFluids.Add("AIR");
                serviceFluids.Add("ANTI FOAM");
                serviceFluids.Add("BIOCIDE");
                serviceFluids.Add("CALORIFIER WATER");
                serviceFluids.Add("CHILLED WATER");
                serviceFluids.Add("COOLING MEDIUM");
                serviceFluids.Add("COOLING WATER");
                serviceFluids.Add("CORR INHIB");
                serviceFluids.Add("CRUDE OIL");
                serviceFluids.Add("CONDENSATE");
                serviceFluids.Add("DEMULSIFIER");
                serviceFluids.Add("DIESEL FUEL");
                serviceFluids.Add("DISTILLED WATER");
                serviceFluids.Add("DRY H/C GAS");
                serviceFluids.Add("ENGINE LO PURIFIER");
                serviceFluids.Add("FLASH GAS");
                serviceFluids.Add("FOAM");
                serviceFluids.Add("FUEL GAS");
                serviceFluids.Add("GAS");
                serviceFluids.Add("GAS INJECTION");
                serviceFluids.Add("GAS LIFT");
                serviceFluids.Add("GLYCOL");
                serviceFluids.Add("GLYCOL/WATER");
                serviceFluids.Add("GRAB SAMPLER");
                serviceFluids.Add("HEAT MEDIUM");
                serviceFluids.Add("HOT WATER");
                serviceFluids.Add("HYDRAULIC OIL");
                serviceFluids.Add("HYDROPHORE");
                serviceFluids.Add("HYPOCHLORITE DEGAS");
                serviceFluids.Add("INSTRUMENT AIR");
                serviceFluids.Add("LIQUID - COOLING MEDIUM");
                serviceFluids.Add("LIQUID - HYDROCARBONS");
                serviceFluids.Add("LO PURIFIER");
                serviceFluids.Add("LUBE OIL");
                serviceFluids.Add("METHANOL");
                serviceFluids.Add("NITROGEN");
                serviceFluids.Add("OIL");
                serviceFluids.Add("OIL EXPORT SYSTEM");
                serviceFluids.Add("OILY WATER");
                serviceFluids.Add("OPEN DRAIN DISCHARGE");
                serviceFluids.Add("OXY SCAV");
                serviceFluids.Add("POLYELECTROLYTE");
                serviceFluids.Add("PORTABLE WATER");
                serviceFluids.Add("PROCESS GAS");
                serviceFluids.Add("PROD WATER DEGASSER");
                serviceFluids.Add("PRODUCED WATER");
                serviceFluids.Add("PROPANE");
                serviceFluids.Add("SCALE INHIB");
                serviceFluids.Add("SEAWATER");
                serviceFluids.Add("SYNTHETIC OIL");
                serviceFluids.Add("VAPOUR (MASS)");
                serviceFluids.Add("WATER FRESH");
                serviceFluids.Add("WATER INJECTION");
                serviceFluids.Add("WAX INHIB");
                serviceFluids.Add("WET H/C GAS");
            }
            return GetRandom2(serviceFluids);
        }

        private dynamic GetRandomPAndId()
        {
            return "WR-P-" + (10 + random.Next(5)) + "-B-PI-000" + (10 + random.Next(50)) + "-00" + random.Next(3);
        }

        static List<string> componentTypes;
        private dynamic GetRandomComponentType()
        {
            if (componentTypes == null)
            {
                componentTypes = new List<string>();
                componentTypes.Add("Air Dryer");
                componentTypes.Add("Aux. Boiler");
                componentTypes.Add("Bottle");
                componentTypes.Add("Compressor");
                componentTypes.Add("Elecric Heater");
                componentTypes.Add("Exchanger");
                componentTypes.Add("Filter");
                componentTypes.Add("Flare Header Line");
                componentTypes.Add("Flare Line");
                componentTypes.Add("Heating Element/Coil");
                componentTypes.Add("Line from Heating Medium Circulating Pump");
                componentTypes.Add("Package");
                componentTypes.Add("Pipe");
                componentTypes.Add("Pipe Relief of Purge Unit");
                componentTypes.Add("Piping System");
                componentTypes.Add("Pressure Vessel");
                componentTypes.Add("Pump");
                componentTypes.Add("Safety & Survival");
                componentTypes.Add("Seal");
                componentTypes.Add("Tank");
                componentTypes.Add("Valve");
            }
            return GetRandom2(componentTypes);
        }

        static List<string> subSystems;
        private dynamic GetRandomSubSystem()
        {
            if (subSystems == null)
            {
                subSystems = new List<string>();
                subSystems.Add("Aviation Fuel System");
                subSystems.Add("Bilge System");
                subSystems.Add("Boiler, Steam and Exhaust System");
                subSystems.Add("Buoy Inflatable Seal");
                subSystems.Add("Cargo System");
                subSystems.Add("Chemical Injection System");
                subSystems.Add("Closed Driain System");
                subSystems.Add("Compressed Air(Instrument Air) System");
                subSystems.Add("Compressed Air(Plant Air) System");
                subSystems.Add("Cooling Medium System");
                subSystems.Add("Dehydration Regeneration System");
                subSystems.Add("Dehydration System");
                subSystems.Add("Diesel and Fuel Oil System");
                subSystems.Add("Essential Generation System");
                subSystems.Add("Fire Water, Foam, Deluge & Sprinkler System");
                subSystems.Add("Flare and Vents System");
                subSystems.Add("Flash Gas Compression System");
                subSystems.Add("Flowline and Riser Systems");
                subSystems.Add("Fresh Water Systems (Potable Water)");
                subSystems.Add("Fuel Gas System");
                subSystems.Add("Gas Export/Re-Injection System");
                subSystems.Add("Gas Lift System");
                subSystems.Add("HVAC System");
                subSystems.Add("Heating Medium System");
                subSystems.Add("Hydraulics System");
                subSystems.Add("Injection Water System");
                subSystems.Add("Lube Oil System");
                subSystems.Add("Main Engine System");
                subSystems.Add("Nitrogen System");
                subSystems.Add("Oil Metering and Export System");
                subSystems.Add("Oil Separation System");
                subSystems.Add("Open Drain System");
                subSystems.Add("Power Generation System");
                subSystems.Add("Produced Water System");
                subSystems.Add("Refridgeration Equipment");
                subSystems.Add("SeaWater Systems");
                subSystems.Add("Turret System");
                subSystems.Add("Wellhead and Manifold Systems");
            }
            return GetRandom2(subSystems);
        }

        static List<string> serviceCategories;
        private dynamic GetRandomServiceCategory()
        {
            if (serviceCategories == null)
            {
                serviceCategories = new List<string>();
                serviceCategories.Add("Air/Nitrogen");
                serviceCategories.Add("Chemical Injection Fluids");
                serviceCategories.Add("Cooling/Heating Medium");
                serviceCategories.Add("Crude Oil & Raw H/C Liquids");
                serviceCategories.Add("Diesel & Marine Gas Oil");
                serviceCategories.Add("Dry Hydrocarbon Gas");
                serviceCategories.Add("Firewater/Foam Systems");
                serviceCategories.Add("Fresh/Potable/Hot Water");
                serviceCategories.Add("Glycol");
                serviceCategories.Add("Hydraulic Oil");
                serviceCategories.Add("Injection Water");
                serviceCategories.Add("Lube Oil");
                serviceCategories.Add("Produced/Oily Water & Drains");
                serviceCategories.Add("Propane & Methanol");
                serviceCategories.Add("Seawater");
                serviceCategories.Add("Steam & Steam Condensate");
                serviceCategories.Add("Wet Hydrocarbon Gas");
            }
            return GetRandom2(serviceCategories);
        }

        private dynamic GetRandomSystem()
        {
            return "A1-01-001-00001-" + (100000 + random.Next(50));
        }

        private dynamic GetRandomReliefSetPressure()
        {
            return random.Next(5000);
        }

        static List<string> locations;
        private dynamic GetRandomLocation()
        {
            if (locations == null)
            {
                locations = new List<string>();
                locations.Add("M01");
                locations.Add("M02");
                locations.Add("M03");
                locations.Add("M07");
                locations.Add("M09");
                locations.Add("M10");
                locations.Add("M11");
                locations.Add("M12");
                locations.Add("M13");
                locations.Add("M14");
                locations.Add("M17");
                locations.Add("M18");
                locations.Add("M45");
                locations.Add("M50");
                locations.Add("M61");
                locations.Add("M64");
                locations.Add("M67");
                locations.Add("M68");
                locations.Add("M72");
                locations.Add("M73");
                locations.Add("M74");
                locations.Add("M75");
                locations.Add("M77");
                locations.Add("T01");
                locations.Add("T03");
                locations.Add("T04");
            }
            return GetRandom2(locations);
        }

        static List<string> plantAreas;
        private dynamic GetRandomPlantArea()
        {
            if (plantAreas == null)
            {
                plantAreas = new List<string>();
                plantAreas.Add("Deck Area");
                plantAreas.Add("Fiscal Metering");
                plantAreas.Add("HeliDeck");
                plantAreas.Add("M01 - Manifolds, Flare KO");
                plantAreas.Add("M02 - HP and MP Separation");
                plantAreas.Add("M03 - LP and Test Separation");
                plantAreas.Add("M06 - Aviation Package");
                plantAreas.Add("M07");
                plantAreas.Add("M09 - LER and HP Compression");
                plantAreas.Add("M10 - LP Flash Gas Comp, Gas Treat");
                plantAreas.Add("M11 - Seawater Deaeration Water Inj");
                plantAreas.Add("M12 - MER, HVAC, Workshop, Lab");
                plantAreas.Add("M13 - Generators");
                plantAreas.Add("M14 - Fiscal Metering");
                plantAreas.Add("M17 - Heating/Cooling Medium Equip");
                plantAreas.Add("M18 - Pipe Rack");
                plantAreas.Add("M45 - Chemical Inj, Utilities");
                plantAreas.Add("M50 - Helideck");
                plantAreas.Add("M61 - Upper Deck Accomodations");
                plantAreas.Add("M64 - Emerg. Gen. Room");
                plantAreas.Add("M67 - Aft Foam Room");
                plantAreas.Add("M68 - Engine Room");
                plantAreas.Add("M72 - Purifier Room");
                plantAreas.Add("M73 - Aft Fire Pump Room");
                plantAreas.Add("M74 - Steering Gear Room");
                plantAreas.Add("M75 - Cargo Pump Room");
                plantAreas.Add("M77 - FWD Foam / Inergen Room");
                plantAreas.Add("T01 - Swivel Access Deck");
                plantAreas.Add("T03 - Mezzanine and Piping Decks");
                plantAreas.Add("T04 - Lower Turret");
            }
            return GetRandom2(plantAreas);
        }

        static List<string> designTypes;
        private static dynamic GetRandomDesignType()
        {
            if (designTypes == null) {
                designTypes = new List<string>();
                designTypes.Add("Conventional");
                designTypes.Add("Bellows");
                designTypes.Add("Pilot Operated");
            }
            return GetRandom2(designTypes);
        }

        private static string GenerateIdentifier(int assetNumber, string prefix = "")
        {
            const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            for (int i = 0; i < 4; i++)
            {
                sb.Append(allowedChars[random.Next(0, allowedChars.Length)]);
            }
            sb.Append("-").AppendFormat("{0:D4}", assetNumber);
            sb.Append("-").AppendFormat("{0:D4}", random.Next(9999));
            return sb.ToString();
        }

        private static Guid GetRandom(Dictionary<Guid, string> values)
        {
            var index = random.Next(values.Count() - 1);
            return values.Keys.ElementAt(index);
        }

        private static Guid GetRandom2(Dictionary<Guid, string> values)
        {
            var index = Math.Sqrt(random.Next((values.Count() - 1) * (values.Count() - 1)));
            return values.Keys.ElementAt((int)index);
        }

        private static dynamic GetRandom2<T>(List<T> values)
        {
            var index = Math.Sqrt(random.Next((values.Count() - 1) * (values.Count() - 1)));
            return values[(int)Math.Round(index)];
        }
    }
}
